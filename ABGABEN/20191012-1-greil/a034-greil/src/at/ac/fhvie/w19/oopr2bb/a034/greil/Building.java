package at.ac.fhvie.w19.oopr2bb.a034.greil;

import java.util.StringJoiner;

/**
 * Erstellen Sie eine Klasse, welche ein Gebäude mit Stockwerken, Anzahl
 * von Aufzügen und ob es ein Bürogebäude ist beschreibt. Ob ein Gebäude ein Bürogebäude ist,
 * kann ein boolscher Wert sein.
 * meine Annahme: "ein Gebäude mit Stockwerken" --> Anzahl Stockwerke
 */

@SuppressWarnings("checkstyle:LineLength")
public class Building {

  private static final int DEFAULT_ELEVATOR_COUNT = 0;
  private static final boolean DEFAULT_OFFICE = false;
  private static final String EX_MESSAGE_ELEVATOR_COUNT = "elevatorCount have to be higher then 0, actual value: ";
  private static final String EX_MESSAGE_FLOOR_COUNT = "floorCount have to be higher then 0, actual value: ";

  private int floorCount;
  private int elevatorCount;
  private boolean office;

  Building(final int floorCount) {
    this(floorCount, DEFAULT_ELEVATOR_COUNT, DEFAULT_OFFICE);
  }

  Building(final int floorCount, final int elevatorCount) {
    this(floorCount, elevatorCount, DEFAULT_OFFICE);
  }

  Building(final int floorCount, final int elevatorCount, final boolean office) {
    /**
     * Gegen eine Zeichenkette zu prüfen ob es einen Fehler gibt ist nicht sehr "sauber"
     * an der Stelle haben wir ja sogar schon die passenden Methoden
     */
    boolean error = !(isValidElevatorCount(floorCount) && isValidFloorCount(elevatorCount));

    if (error) {
      String errorMessage = "";
      if (!isValidElevatorCount(floorCount)) {
        errorMessage = getElevatorCountExceptionMessage(elevatorCount);
      }
      if (!isValidFloorCount(elevatorCount)) {
        errorMessage += getFloorCountExceptionMessage(floorCount);
      }

      throw new IllegalArgumentException(errorMessage);
    }

    this.floorCount = floorCount;
    this.elevatorCount = elevatorCount;
    this.office = office;
  }

  public int getFloorCount() {
    return floorCount;
  }

  public void setFloorCount(final int floorCount) {
    if (!isValidFloorCount(floorCount)) {
      throw new IllegalArgumentException(getFloorCountExceptionMessage(floorCount));
    }
    this.floorCount = floorCount;
  }

  public int getElevatorCount() {
    return elevatorCount;
  }

  public void setElevatorCount(final int elevatorCount) {
    if (!isValidElevatorCount(elevatorCount)) {
      throw new IllegalArgumentException(getElevatorCountExceptionMessage(elevatorCount));
    }
    this.elevatorCount = elevatorCount;
  }

  public boolean isOffice() {
    return office;
  }

  public void setOffice(final boolean office) {
    this.office = office;
  }

  private String getFloorCountExceptionMessage(final int value) {
    return EX_MESSAGE_FLOOR_COUNT + value + "\n";
  }

  private String getElevatorCountExceptionMessage(final int value) {
    return EX_MESSAGE_ELEVATOR_COUNT + value + "\n";
  }

  private boolean isValidFloorCount(final int value) {
    return (value >= 0);
  }

  private boolean isValidElevatorCount(final int value) {
    return (value >= 0);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Building.class.getSimpleName() + "[", "]")
        .add("floorCount: " + floorCount)
        .add("elevatorCount: " + elevatorCount)
        .add("office: " + office)
        .toString();
  }
}

/*
@startuml
class Building{
- {static} EX_MESSAGE_ELEVATOR_COUNT : String = "elevatorCount have to be higher then 0, actual value: "
- {static} EX_MESSAGE_FLOOR_COUNT : String = "floorCount have to be higher then 0, actual value: "
- {static} DEFAULT_ELEVATOR_COUNT : int = 0
- {static} DEFAULT_OFFICE : boolean = false
- floorCount: int {>=0}
- elevatorCount: int {>=0}
- office : boolean
--
+ Building(floorCount : int)
+ Building(floorCount : int, elevatorCount : int)
+ Building(floorCount : int, elevatorCount : int, office : boolean)
--
+ getFloorCount() : int
+ setFloorCount(floorCount: int)
+ setElevatorCount() : int
+ getElevatorCount(elevatorCount: int)
+ setOffice() : boolean
+ isOffice(office : boolean)
+ toString()
--
- isValidFloorCount(int value) : boolean
- getFloorCountExceptionMessage(int value) : String
- isValidElevatorCount(int value) : boolean
- getElevatorCountExceptionMessage(int value) : String
}
@enduml
 */