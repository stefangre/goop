package src.at.ac.fhvie.w19.oopr2bb.a035.greil;

class Controller {

  private static final String MODEL = "KNOPPARP";
  private static final String MODEL2 = "SANELA";
  private static final String MODEL3 = "SKOGSKORN";
  private static final String COLOR = "Red";
  private static final String COLOR2 = "Blue";
  private static final int WEIGHT = 10;

  private static final String ILLEGAL_VALUE_MODEL = "";
  private static final String ILLEGAL_VALUE_COLOR = "";
  private static final int ILLEGAL_VALUE_WEIGHT = -2;

  void create() {
    Furniture furniture1 = new Furniture(MODEL);
    Furniture furniture2 = new Furniture(MODEL2, COLOR);
    Furniture furniture3 = new Furniture(MODEL3, COLOR2, WEIGHT);
    System.out.println(furniture1);
    System.out.println(furniture2);
    System.out.println(furniture3);

    try {
      new Furniture(ILLEGAL_VALUE_MODEL, ILLEGAL_VALUE_COLOR, ILLEGAL_VALUE_WEIGHT);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
      System.out.println("Exit");
      System.exit(1);
    }
  }
}
