package src.at.ac.fhvie.w19.oopr2bb.a035.greil;

public class Main {
  public static void main(final String[] args) {
    // Erzeugen einer Instanz der Businesslogik
    Controller ctrl = new Controller();
    // Starten der Businesslogik
    ctrl.create();
  }
}
