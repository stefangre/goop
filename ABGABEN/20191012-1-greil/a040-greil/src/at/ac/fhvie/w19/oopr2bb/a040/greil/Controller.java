package at.ac.fhvie.w19.oopr2bb.a040.greil;

class Controller {

  private static final String MANUFRACTURER = "Asus";
  private static final String MANUFRACTURER2 = "Google";
  private static final String MANUFRACTURER3 = "Apple";
  private static final String OS = "Chrome OS";
  private static final String OS2 = "OSX";
  private static final double FREQUENCE = 3.4;

  private static final String ILLEGAL_VALUE_MANUFRACTURER = "";
  private static final String ILLEGAL_VALUE_OS = "";
  private static final double ILLEGAL_VALUE_FREQUENCE = -2.0;

  void create() {
    Computer computer1 = new Computer(MANUFRACTURER);
    Computer computer2 = new Computer(MANUFRACTURER2, OS);
    Computer computer3 = new Computer(MANUFRACTURER3, OS2, FREQUENCE);
    System.out.println(computer1);
    System.out.println(computer2);
    System.out.println(computer3);

    try {
      new Computer(ILLEGAL_VALUE_MANUFRACTURER, ILLEGAL_VALUE_OS, ILLEGAL_VALUE_FREQUENCE);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
      System.out.println("Exit");
      System.exit(1);
    }
  }
}
