package at.ac.fhvie.w19.oopr2bb.a043.greil;

class Controller {

  private static final double AREA = 100.0;
  private static final double AREA2 = 90.0;
  private static final double AREA3 = 110.5;
  private static final int ROOM_COUNT = 3;
  private static final int ROOM_COUNT2 = 5;
  private static final int WINDOW_COUNT = 25;

  private static final double ILLEGAL_VALUE_AREA = -5;
  private static final int ILLEGAL_VALUE_ROOM_COUNT = -2;
  private static final int ILLEGAL_VALUE_WINDOW_COUNT = -20;

  void create() {
    Apartment apartment1 = new Apartment(AREA);
    Apartment apartment2 = new Apartment(AREA2, ROOM_COUNT);
    Apartment apartment3 = new Apartment(AREA3, ROOM_COUNT2, WINDOW_COUNT);
    System.out.println(apartment1);
    System.out.println(apartment2);
    System.out.println(apartment3);

    try {
      new Apartment(ILLEGAL_VALUE_AREA, ILLEGAL_VALUE_ROOM_COUNT, ILLEGAL_VALUE_WINDOW_COUNT);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
      System.out.println("Exit");
      System.exit(1);
    }
  }
}
