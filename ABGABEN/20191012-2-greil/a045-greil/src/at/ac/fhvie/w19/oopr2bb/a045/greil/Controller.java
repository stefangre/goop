package at.ac.fhvie.w19.oopr2bb.a045.greil;

class Controller {

  private static final String DESCRIPTION = "Cat";
  private static final String DESCRIPTION2 = "Tiger";
  private static final String DESCRIPTION3 = "Lion";
  private static final int GESTATION_IN_DAYS = 100;
  private static final int GESTATION_IN_DAYS2 = 110;
  private static final double AVARAGE_BIRTHS = 1.5;

  private static final String ILLEGAL_VALUE_DESCRIPTION = "";
  private static final int ILLEGAL_VALUE_GESTATION_IN_DAYS = -1;
  private static final double ILLEGAL_VALUE_AVARAGE_BIRTHS = -20.5;

  void create() {
    Mammal mammal1 = new Mammal(DESCRIPTION);
    Mammal mammal2 = new Mammal(DESCRIPTION2, GESTATION_IN_DAYS);
    Mammal mammal3 = new Mammal(DESCRIPTION3, GESTATION_IN_DAYS2, AVARAGE_BIRTHS);
    System.out.println(mammal1);
    System.out.println(mammal2);
    System.out.println(mammal3);

    try {
      new Mammal(ILLEGAL_VALUE_DESCRIPTION, ILLEGAL_VALUE_GESTATION_IN_DAYS, ILLEGAL_VALUE_AVARAGE_BIRTHS);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
      System.out.println("Exit");
      System.exit(1);
    }
  }
}
