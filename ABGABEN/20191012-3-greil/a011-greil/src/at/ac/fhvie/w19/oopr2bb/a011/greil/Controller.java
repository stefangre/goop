package at.ac.fhvie.w19.oopr2bb.a011.greil;

class Controller {

  private static final String MODEL = "KNOPPARP";
  private static final String MODEL2 = "SANELA";
  private static final String MODEL3 = "SKOGSKORN";
  private static final int HEIGHT = 20;
  private static final int HEIGHT2 = 30;
  private static final int WIDTH = 10;

  private static final String ILLEGAL_VALUE_MODEL = "";
  private static final int ILLEGAL_VALUE_HEIGHT = -5;
  private static final int ILLEGAL_VALUE_WIDTH = -2;

  void create() {
    Furniture furniture1 = new Furniture(MODEL);
    Furniture furniture2 = new Furniture(MODEL2, HEIGHT);
    Furniture furniture3 = new Furniture(MODEL3, HEIGHT2, WIDTH);
    System.out.println(furniture1);
    System.out.println(furniture2);
    System.out.println(furniture3);

    try {
      new Furniture(ILLEGAL_VALUE_MODEL, ILLEGAL_VALUE_HEIGHT, ILLEGAL_VALUE_WIDTH);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
      System.out.println("Exit");
      System.exit(1);
    }
  }
}
