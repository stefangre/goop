package at.ac.fhvie.w19.oopr2bb.a014.greil;

class Controller {
  private static final String ADDRESS = "Wohlmutstraße 22";
  private static final String ADDRESS2 = "Universitätsring 1";
  private static final String ADDRESS3 = "Wexstraße 19–23";
  private static final int ELEVATOR_COUNT = 5;
  private static final int ELEVATOR_COUNT2 = 10;
  private static final int FLOOR_COUNT = 16;

  private static final String ILLEGAL_VALUE_ADDRESS = "";
  private static final int ILLEGAL_VALUE_ELEVATOR_COUNT = -2;
  private static final int ILLEGAL_VALUE_FLOOR_COUNT = -2;

  void create() {
    Building building1 = new Building(ADDRESS);
    Building building2 = new Building(ADDRESS2, ELEVATOR_COUNT);
    Building building3 = new Building(ADDRESS3, ELEVATOR_COUNT2, FLOOR_COUNT);
    System.out.println(building1);
    System.out.println(building2);
    System.out.println(building3);

    try {
      new Building(ILLEGAL_VALUE_ADDRESS, ILLEGAL_VALUE_ELEVATOR_COUNT, ILLEGAL_VALUE_FLOOR_COUNT);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
      System.out.println("Exit");
      System.exit(1);
    }
  }
}
