package at.ac.fhvie.w19.oopr2bb.a020.greil;

import java.util.StringJoiner;

@SuppressWarnings("checkstyle:LineLength")
public class Computer {

  private static final double DEFAULT_FREQUENCY = 1.0;
  private static final String DEFAULT_OS = "Linux";
  private static final String EX_MESSAGE_PORT = "portCount have to be higher then 0, actual value:  \n";
  private static final String EX_MESSAGE_OS = "operatingSystem must not be empty. \n";
  private static final String EX_MESSAGE_FREQUENCY = "frequency have to be higher then 0.0, actual value: ";

  private int portCount;
  private String operatingSystem;
  private double frequency;

  Computer(final int portCount) {
    this(portCount, DEFAULT_OS, DEFAULT_FREQUENCY);
  }

  Computer(final int portCount, final String operatingSystem) {
    this(portCount, operatingSystem, DEFAULT_FREQUENCY);
  }

  Computer(final int portCount, final String operatingSystem, final double frequency) {
    boolean error = !(isValidPortCount(
        portCount) && isValidOperatingSystem(operatingSystem) && isValidFrequency(frequency));

    if (error) {
      String errorMessage = "";
      if (!isValidPortCount(portCount)) {
        errorMessage = getPortCountExceptionMessage(portCount);
      }
      if (!isValidOperatingSystem(operatingSystem)) {
        errorMessage += getOperatingSystemExceptionMessage();
      }
      if (!isValidFrequency(frequency)) {
        errorMessage += getFrequencyExceptionMessage(frequency);
      }
      throw new IllegalArgumentException(errorMessage);
    }

    this.portCount = portCount;
    this.operatingSystem = operatingSystem;
    this.frequency = frequency;
  }

  public int getPortCount() {
    return portCount;
  }

  public void setPortCount(final int portCount) {
    if (!isValidPortCount(portCount)) {
      throw new IllegalArgumentException(getPortCountExceptionMessage(portCount));
    }
    this.portCount = portCount;
  }

  public String getOperatingSystem() {
    return operatingSystem;
  }

  public void setOperatingSystem(final String operatingSystem) {
    if (!isValidOperatingSystem(operatingSystem)) {
      throw new IllegalArgumentException(getOperatingSystemExceptionMessage());
    }
    this.operatingSystem = operatingSystem;
  }

  public double getFrequency() {
    return frequency;
  }

  public void setFrequency(final double frequency) {
    if (!isValidFrequency(frequency)) {
      throw new IllegalArgumentException(getFrequencyExceptionMessage(frequency));
    }
    this.frequency = frequency;
  }

  private boolean isValidPortCount(final int value) {
    return value > 0;
  }

  private boolean isValidOperatingSystem(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidFrequency(final double value) {
    return value > 0;
  }

  private String getPortCountExceptionMessage(final int value) {
    return EX_MESSAGE_PORT + value + "\n";
  }

  private String getOperatingSystemExceptionMessage() {
    return EX_MESSAGE_OS;
  }

  private String getFrequencyExceptionMessage(final double value) {
    return EX_MESSAGE_FREQUENCY + value + "\n";
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Computer.class.getSimpleName() + "[", "]")
        .add("portCount=" + portCount)
        .add("operatingSystem='" + operatingSystem + "'")
        .add("frequency=" + frequency)
        .toString();
  }
}

/*
@startuml
class Computer{
- {static} DEFAULT_FREQUENCY : double = 1.0
- {static} DEFAULT_OS : String = "Linux"
- {static} EX_MESSAGE_MANUFRACTURER : String = "manufacturer must not be empty."
- {static} EX_MESSAGE_OS : String = "operatingSystem must not be empty."
- {static} EX_MESSAGE_FREQUENCY : String = "frequency have to be higher then 0, actual value: "
- portCount: String {>0}
- operatingSystem: String {notNull, notEmpty}
- frequency : double {>0}
--
+ Computer(portCount : int)
+ Computer(portCount : int, operatingSystem : String)
+ Computer(portCount : int, operatingSystem : String, frequency : double)
--
+ getPortCount() : int
+ setPortCount(portCount: int)
+ setOperatingSystem() : String
+ getOperatingSystem(operatingSystem: String)
+ setFrequency() : double
+ getFrequency(frequency : double)
+ toString() : String
--
- isValidPortCount(value: int) : boolean
- getPortCountExceptionMessage(value : int) : String
- isValidOperatingSystem(value: String) : boolean
- getOperatingSystemExceptionMessage() : String
- isValidFrequency(value: double) : boolean
- getFrequencyExceptionMessage(value: double) : String
}
@enduml
 */