package at.ac.fhvie.w19.oopr2bb.a070.greil;

import java.util.ArrayList;
import java.util.Arrays;

class Controller {

  private static final boolean USEFULL = true;
  private static final boolean NOT_USEFULL = false;

  void create() {
    KitchenUtensil teaKettle = new KitchenUtensil(USEFULL);
    KitchenUtensil pan = new KitchenUtensil(USEFULL);
    KitchenUtensil woodenSpoon = new KitchenUtensil(NOT_USEFULL);
    KitchenUtensil saucePan = new KitchenUtensil(USEFULL);
    KitchenUtensil toaster = new KitchenUtensil(NOT_USEFULL);

    //add all Instances to the ArrayList
    ArrayList<KitchenUtensil> kitchenUtensils = new ArrayList<>(Arrays.asList(teaKettle, pan, woodenSpoon, saucePan, toaster));
    ArrayList<KitchenUtensil> notUsefulKitchenUtensils = new ArrayList<>();

    //Iterate trough the List and print out the objects which where useful
    //and add the not useful ones to the notUseful-ArrayList
    for (KitchenUtensil kitchenUtensil : kitchenUtensils) {
      if (kitchenUtensil.isUseful()) {
        System.out.println(kitchenUtensil);
      } else {
        notUsefulKitchenUtensils.add(kitchenUtensil);
      }
    }

    //remove the Elements in notUsefulKitchenUtensils from kitchenUtensils
    kitchenUtensils.removeAll(notUsefulKitchenUtensils);
    System.out.println(kitchenUtensils);
  }
}
