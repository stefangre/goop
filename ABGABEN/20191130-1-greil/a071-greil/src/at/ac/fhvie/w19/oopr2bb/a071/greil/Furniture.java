package at.ac.fhvie.w19.oopr2bb.a071.greil;

import java.util.ArrayList;

public class Furniture {

  private final ArrayList<Chair> chairs = new ArrayList<>();

  public Furniture(final ArrayList<Chair> chairs) {

    if (!isValidChairs(chairs)) {
      throw new IllegalArgumentException(getChairsExceptionMessage(chairs));
    }

    ArrayList<Chair> values = new ArrayList<>(chairs);
    this.chairs.addAll(values);
  }

  public ArrayList<Chair> getChairs() {
    return new ArrayList<Chair>(chairs);
  }

  private boolean isValidChairs(final ArrayList<Chair> values) {
    return (!(values == null || values.isEmpty()));
  }

  private String getChairsExceptionMessage(final ArrayList<Chair> values) {
    String errorMessage = "";

    if (values == null) {
      errorMessage = "List is null.";
      //hier wird in der (Musterlösung) ein else if benötigt (oder gleich retournierung des Strings)
      // sonst wird bei Instanziierung mit null eine NullPointerException geworfen wenn er versucht
      // im zweiten if (values.size() == 0) zu prüfen
    } else if (values.isEmpty()) { //isEmpty
      errorMessage = "List is empty, please add at least one item to the list.";
    }
    return errorMessage;
  }
}
