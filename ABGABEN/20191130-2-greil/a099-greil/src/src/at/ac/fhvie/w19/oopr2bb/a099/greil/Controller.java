package src.at.ac.fhvie.w19.oopr2bb.a099.greil;

class Controller {

  void create() {
    School ahs = new School(Type.AHS);
    School htl = new School(Type.HTL);
    School nms = new School(Type.NMS);

    System.out.println(ahs);
    System.out.println(htl);
    System.out.println(nms);

  }
}
