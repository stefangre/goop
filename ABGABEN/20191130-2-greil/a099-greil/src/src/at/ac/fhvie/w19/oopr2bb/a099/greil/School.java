package src.at.ac.fhvie.w19.oopr2bb.a099.greil;

import java.util.StringJoiner;

public class School {

  private static final int START_COUNTER = 101002;
  private static int idCounter = START_COUNTER;
  private int id;
  private Type type;

  public School(final Type type) {
    this.type = type;
    this.id = idCounter++;
  }

  public int getId() {
    return id;
  }

  public Type getType() {
    return type;
  }

  public void setType(final Type type) {
    this.type = type;
  }

  @Override
  public String toString() {
    return "Schultyp: " + type.getDescription() + id;
  }
}

/*
@startuml
class School{
- {static} START_COUNTER : int = 101002
- {static} idCounter : int = START_COUNTER
- id : int
--
+ School(type: Type)
--
+ getId() : int
+ setType() : Type
+ getType(type: Type)
}
@enduml
 */
