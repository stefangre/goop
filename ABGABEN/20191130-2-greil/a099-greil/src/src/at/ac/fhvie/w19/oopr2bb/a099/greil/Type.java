package src.at.ac.fhvie.w19.oopr2bb.a099.greil;

//Unsicher ob das so gewollt war,
//falls die Abkürzung und der volle Name hier gemischt gehören wäre es einfach
//HTL, AHS, NEUE_MITTELSCHULE;

public enum Type {
  HTL("Höhere Technische Lehranstalt"),
  AHS("Allgemein bildende höhere Schule"),
  NMS("Neue Mittelschule");

  private String description;

  public String getDescription() {
    return description;
  }

  Type(String description) {
    this.description = description;
  }
}
/*
@startuml
enum Type{
  HTL(String description)
  AHS(String description)
  NMS(String description)
- description : String
--
+ Type(String description)
--
}
@enduml
 */