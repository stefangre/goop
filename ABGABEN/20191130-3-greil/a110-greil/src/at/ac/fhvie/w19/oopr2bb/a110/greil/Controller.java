package at.ac.fhvie.w19.oopr2bb.a110.greil;

class Controller {

  void create() {
    Bedclothes satin = new Bedclothes(Material.SATIN);
    Bedclothes leinen = new Bedclothes(Material.LEINEN);

    System.out.println(satin);
    System.out.println(leinen);
    System.out.println(leinen.getMaterial().getDescription());
    System.out.println(leinen.getMaterial().getPriceClass());

  }
}
