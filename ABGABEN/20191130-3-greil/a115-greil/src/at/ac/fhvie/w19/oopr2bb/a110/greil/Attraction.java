package at.ac.fhvie.w19.oopr2bb.a110.greil;

import java.util.Objects;
import java.util.StringJoiner;

public class Attraction implements Comparable {
  private static final String EX_MESSAGE_NAME = "Name must not be empty. \n";
  private static final String EX_MESSAGE_VISITOR_COUNT =
      "Besucheranzahl des letzten Jahres have to be higher than 0";
  private static final int DEFAULT_VISITOR_COUNT = 0;
  private static int idCounter = 0;

  /*
  "• Erstellen Sie eine Möglichkeit die Sehenswürdigkeiten, wenn sie in einer Collection
gespeichert sind, nach ID entweder aufsteigend oder absteigend zu ordnen."

--> löse es mit einer Id auf der Mutterklasse somit ergibt sich kein Problem mit Id-Duplikaten bzw
    einer Logik zu Prüfen ob es schon duplikate gibt
    Subklassen bei denen in der Angabe eine zusätzliche Id gefordet wurde haben eine
   */
  private final int globalId;
  private String name;
  private int lastYearVisitorCount;

  public Attraction(final String name) {
    this(name, DEFAULT_VISITOR_COUNT);
  }

  public Attraction(final String name, final int lastYearVisitorCount) {
    String errorMessage = "";

    if (!isValidName(name)) {
      errorMessage += EX_MESSAGE_NAME;
    }
    if (!isValidPosNumber(lastYearVisitorCount)) {
      errorMessage += EX_MESSAGE_VISITOR_COUNT;
    }
    if (!errorMessage.isEmpty()) {
      throw new IllegalArgumentException(errorMessage);
    }
    this.globalId = idCounter++;
    this.name = name;
    this.lastYearVisitorCount = lastYearVisitorCount;
  }

  private boolean isValidName(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  public int getGlobalId() {
    return globalId;
  }

  public boolean isValidPosNumber(final int value) {
    return value >= 0;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public int getLastYearVisitorCount() {
    return lastYearVisitorCount;
  }

  public void setLastYearVisitorCount(final int lastYearVisitorCount) {
    this.lastYearVisitorCount = lastYearVisitorCount;
  }

  @Override
  public boolean equals(final Object value) {
    if (this == value) {
      return true;
    }
    if (value == null || getClass() != value.getClass()) {
      return false;
    }
    Attraction that = (Attraction) value;
    return lastYearVisitorCount == that.lastYearVisitorCount && globalId == that.globalId
        && Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, lastYearVisitorCount, globalId);
  }

  public int compareTo(final Attraction value) {
    if (this.globalId > value.globalId) {
      return 1;
    } else if (this.globalId == value.globalId) {
      return 0;
    } else {
      return -1;
    }
  }

  @Override
  public int compareTo(final Object value) {
    return 0;
  }

  @Override
  public String toString() {
    //[type=love,smell=turpentine,look=india ink]
    return new StringJoiner(", ", "[", "]")
        .add("globalId=" + globalId)
        .add("name='" + name + "'")
        .add("lastYearVisitorCount=" + lastYearVisitorCount)
        .toString();
  }
}

/*
@startuml
interface Comparable


class Attraction implements Comparable {
- {static} EX_MESSAGE_NAME : String = "Name must not be empty."
- {static} EX_MESSAGE_VISITOR_COUNT : String = "Besucheranzahl des letzten Jahres have to be ..."
- {static} idCounter : int = 0
- {static} DEFAULT_VISITOR_COUNT : int = 0
- globalId : int
- name : String
- lastYearVisitorCount : int
--
+ Attraction(name: String)
+ Attraction(name: String, lastYearVisitorCount: int)
--
+ getGlobalId() : int
+ isValidPosNumber(int value)
+ getName() : String
+ setName(name : String)
+ getLastYearVisitorCount() : int
+ setLastYearVisitorCount(lastYearVisitorCount : int)
+ equals(Object) : boolean
+ hashCode() : int
+ compareTo(other : Attraction) : int
+ compareTo(value : Object) : int
+ toString() : String
--
- isValidName(value : String) : boolean
}

class Castle extends Attraction {
- {static} EX_MESSAGE_CONSTRUCTION_YEAR : String =  "The construction year have to be higher ..."
- {static} DEFAULT_CONSTRUCTION_YEAR : int = 1991
- {static} START_COUNTER : int = 3200
- {static} idCounter : int = START_COUNTER
- id : int
- constructionYear : int
--
+ Castle(name: String, lastYearVisitorCount int)
+ Castle(name: String, lastYearVisitorCount: int, constructionYear : int)
--
+ getConstructionYear() : int
+ setConstructionYear(constructionYear : int)
+ getId() : int
+ toString() : String
}

class Museum extends Attraction {
- {static} DEFAULT_ALIGMENT : Alignment = Alignment.NATURWISSENSCHAFT
- {static} idCounter : int = 1
- id : int
- alignment : Alignment
--
+ Museum(name: String, lastYearVisitorCount int)
+ Museum(name: String, lastYearVisitorCount: int, alignment : Alignment)
--
+ getAlignment() : Alignment
+ setAlignment(alignment : Alignment)
+ getId() : int
+ toString() : String
}

class ObservationTower extends Attraction {
- {static} EX_MESSAGE_DEFAULT_HEIGHT : String = "The height have to be higher than 0"
- {static} DEFAULT_HEIGHT : int = 100
- heightInMeters : int
--
+ ObservationTower(name: String, lastYearVisitorCount int)
+ ObservationTower(name: String, lastYearVisitorCount: int, heightInMeters : int)
--
+ getHeightInMeters() : int
+ setHeightInMeters(heightInMeters : int)
+ toString() : String
}

class Zoo extends Attraction {
- {static} EX_MESSAGE_ANIMAL_TYPE_COUNTER : String = "The animal-type-counter have to b..."
- {static} DEFAULT_ANIMAL_TYPE_COUNTER : int = 100
- {static} START_COUNTER : int = 3200
- {static} idCounter : int = START_COUNTER
- id : int
- animalTypeCounter : int
--
+ Zoo(name: String, lastYearVisitorCount int)
+ Zoo(name: String, lastYearVisitorCount: int, animalTypeCounter : int)
--
+ getAnimalTypeCounter() : int
+ setAnimalTypeCounter(animalTypeCounter : int)
+ getId() : int
+ toString() : String
}


Museum ..> Alignment : <<use>>

enum Alignment{
  KUST
  NATURWISSENSCHAFT
  GESCHICHTE
}

class AttractionGlobalIdComparator implements Comparator {
--
+ compare(left: Attraction, right Attraction) : int
}
@enduml
 */
