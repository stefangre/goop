package at.ac.fhvie.w19.oopr2bb.a110.greil;

import java.util.StringJoiner;

public class Castle extends Attraction {

  private static final String EX_MESSAGE_CONSTRUCTION_YEAR =
      "The construction year have to be higher than 0";
  private static final int DEFAULT_CONSTRUCTION_YEAR = 1991;
  private static final int START_COUNTER = 3200;
  private static int idCounter = START_COUNTER;
  private final int id;
  private int constructionYear;

  public Castle(final String name, final int lastYearVisitorCount) {
    this(name, lastYearVisitorCount, DEFAULT_CONSTRUCTION_YEAR);
  }

  public Castle(final String name, final int lastYearVisitorCount, final int constructionYear) {
    super(name, lastYearVisitorCount);
    if (!super.isValidPosNumber(constructionYear)) {
      throw new IllegalArgumentException(EX_MESSAGE_CONSTRUCTION_YEAR);
    }
    this.constructionYear = constructionYear;
    this.id = idCounter++;
  }

  public int getConstructionYear() {
    return constructionYear;
  }

  public void setConstructionYear(final int constructionYear) {
    this.constructionYear = constructionYear;
  }

  public int getId() {
    return id;
  }

  @Override
  public String toString() {
    // {[type=love], [smell=turpentine], [look=india ink]}
    return new StringJoiner(", ", "{", "}")
        .add("[globalId=" + getGlobalId() + "]")
        .add("[id=" + id + "]")
        .add("[constructionYear=" + constructionYear + "]")
        .toString();
  }
}

/*
@startuml
class Castle extends Attraction {
- {static} EX_MESSAGE_CONSTRUCTION_YEAR : String =  "The construction year have to be higher ..."
- {static} DEFAULT_CONSTRUCTION_YEAR : int = 1991
- {static} START_COUNTER : int = 3200
- {static} idCounter : int = START_COUNTER
- id : int
- constructionYear : int
--
+ Castle(name: String, lastYearVisitorCount int)
+ Castle(name: String, lastYearVisitorCount: int, constructionYear : int)
--
+ getConstructionYear() : int
+ setConstructionYear(constructionYear : int)
+ toString() : String
}
@enduml
 */
