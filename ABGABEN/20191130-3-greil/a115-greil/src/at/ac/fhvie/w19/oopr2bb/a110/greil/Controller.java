package at.ac.fhvie.w19.oopr2bb.a110.greil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

class Controller {

  private static final int VISITOR_COUNT_RIESENRAD = 1000000;
  private static final int VISITOR_COUNT_STEPHANSDOM = 10000084;
  private static final int VISITOR_COUNT_KUNST = 1000004;
  private static final int VISITOR_COUNT_NATUR = 300000;
  private static final int VISITOR_COUNT_BELVEDERE = 3000000;
  private static final int VISITOR_COUNT_SCHOENBRUNN = 9000000;
  private static final int VISITOR_COUNT_DONAUTURM = 8400;
  private static final int VISITOR_COUNT_JUBILAEUMSWARTE = 1840;
  private static final int VISITOR_COUNT_TIERGARTEN_SCHOENBRUNN = 800040;
  private static final int VISITOR_COUNT_HAUS_DES_MEERES = 80004;
  private static final int CONSTRUCTION_YEAR_BELVEDERE = 1850;
  private static final int CONSTRUCTION_YEAR_SCHOENBRUNN = 1638;
  private static final int HEIGHT_DONAUTURM = 500;
  private static final int HEIGHT_JUBILAEUMSWARTE = 400;
  private static final int ANIMAL_TYPE_COUNTER_TS = 200;
  private static final int ANIMAL_TYPE_COUNTER_HDM = 100;

  void create() {
    Attraction riesenRad = new Attraction("Riesenrad", VISITOR_COUNT_RIESENRAD);
    Attraction stephansDom = new Attraction("Stephansdom", VISITOR_COUNT_STEPHANSDOM);

    Museum kust = new Museum("Kust", VISITOR_COUNT_KUNST, Alignment.KUST);
    Museum natur =
        new Museum("Natur", VISITOR_COUNT_NATUR, Alignment.NATURWISSENSCHAFT);

    Castle belvedere =
        new Castle("Belvedere", VISITOR_COUNT_BELVEDERE, CONSTRUCTION_YEAR_BELVEDERE);
    Castle schoenbrunn =
        new Castle("Schönbrunn", VISITOR_COUNT_SCHOENBRUNN, CONSTRUCTION_YEAR_SCHOENBRUNN);

    ObservationTower donauturm =
        new ObservationTower("Donauturm", VISITOR_COUNT_DONAUTURM, HEIGHT_DONAUTURM);
    ObservationTower jubilaeumswarte = new ObservationTower("Jubiläumswarte",
        VISITOR_COUNT_JUBILAEUMSWARTE, HEIGHT_JUBILAEUMSWARTE);

    Zoo tiergartenSchoenbrunn =
        new Zoo("Tiergarten Schönbrunn", VISITOR_COUNT_TIERGARTEN_SCHOENBRUNN,
            ANIMAL_TYPE_COUNTER_TS);
    Zoo hausDesMeeres = new Zoo("Haus des Meeres",
        VISITOR_COUNT_HAUS_DES_MEERES, ANIMAL_TYPE_COUNTER_HDM);

    ArrayList<Attraction> attractions = new ArrayList<>(Arrays.asList(riesenRad, stephansDom, kust,
        natur, belvedere, schoenbrunn, donauturm, jubilaeumswarte, tiergartenSchoenbrunn,
        hausDesMeeres));

    AttractionGlobalIdComparator attractionGlobalIdComparator = new AttractionGlobalIdComparator();

    //aufsteigend nach globalId
    Collections.sort(attractions, attractionGlobalIdComparator);
    System.out.println(attractions);
    //absteigend nach globalId
    Collections.sort(attractions, attractionGlobalIdComparator.reversed());
    System.out.println(attractions);

  }
}