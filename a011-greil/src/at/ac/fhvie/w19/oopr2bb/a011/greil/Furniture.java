package at.ac.fhvie.w19.oopr2bb.a011.greil;

import java.util.StringJoiner;

@SuppressWarnings("checkstyle:LineLength")
class Furniture {

  private static final int DEFAULT_WIDTH = 20;
  private static final int DEFAULT_HEIGHT = 10;
  private static final String EX_MESSAGE_MODEL = "model must not be empty. \n";
  private static final String EX_MESSAGE_WIDTH = "width have to be higher than 0, actual value: ";
  private static final String EX_MESSAGE_HEIGHT = "height have to be higher than 0, actual value: ";

  private String model;
  private int width;
  private int height;

  Furniture(final String model) {
    this(model, DEFAULT_WIDTH, DEFAULT_HEIGHT);
  }

  Furniture(final String model, final int width) {
    this(model, width, DEFAULT_HEIGHT);
  }

  Furniture(final String model, final int width, final int height) {
    boolean error = !(isValidModel(model) && isValidWidth(width) && isValidHeight(height));

    if (error) {
      String errorMessage = "";
      if (!isValidModel(model)) {
        errorMessage = getModelExceptionMessage();
      }
      if (!isValidWidth(width)) {
        errorMessage += getWidthExceptionMessage(width);
      }
      if (!isValidHeight(height)) {
        errorMessage += getHeightExceptionMessage(height);
      }
      throw new IllegalArgumentException(errorMessage);
    }

    this.model = model;
    this.width = width;
    this.height = height;
  }


  public String getModel() {
    return model;
  }

  public void setModel(final String model) {
    if (!isValidModel(model)) {
      throw new IllegalArgumentException(getModelExceptionMessage());
    }
    this.model = model;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(final int width) {
    if (!isValidWidth(width)) {
      throw new IllegalArgumentException(getWidthExceptionMessage(width));
    }
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(final int height) {
    if (!isValidHeight(height)) {
      throw new IllegalArgumentException(getHeightExceptionMessage(height));
    }
    this.height = height;
  }

  private boolean isValidModel(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidWidth(final int value) {
    return value >= 0;
  }

  private boolean isValidHeight(final int value) {
    return value >= 0;
  }

  private String getModelExceptionMessage() {
    return EX_MESSAGE_MODEL;
  }

  private String getWidthExceptionMessage(final int value) {
    return EX_MESSAGE_WIDTH + value + "\n";
  }

  private String getHeightExceptionMessage(final int value) {
    return EX_MESSAGE_HEIGHT + value + "\n";
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Furniture.class.getSimpleName() + "[", "]")
        .add("model='" + model + "'")
        .add("width=" + width)
        .add("height=" + height)
        .toString();
  }
}
/*
@startuml
class Furniture{
- {static} EX_MESSAGE_MODEL : String = "model must not be empty"
- {static} EX_MESSAGE_WIDTH : String = "width have to be higher than 0, actual value: "
- {static} EX_MESSAGE_HEIGHT : String = "height have to be higher than 0, actual value: "
- {static} DEFAULT_WIDTH : int = 20
- {static} DEFAULT_HEIGHT : int = 10
- model: String {notNull, notEmpty}
- width: int {>0}
- height : int {>0}
--
+ Furniture(model : String)
+ Furniture(model : String, width : int)
+ Furniture(model : String, width : int, height : int)
--
+ getModel() : String
+ setModel(model: String)
+ setWidth() : int
+ getWidth(width: int)
+ setHeight() : int
+ getHeight(height : int)
+ toString()
--
- isValidModel(value: String) : boolean
- getModelExceptionMessage() : String
- isValidWidth(value: int) : boolean
- getWidthExceptionMessage(value: int) : String
- isValidHeight(value: int) : boolean
- getHeightExceptionMessage(value: int)) : String
}
@enduml
 */
