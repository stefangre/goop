package at.ac.fhvie.w19.oopr2bb.a011.greil;

// Lektor: UML bei width ist der Datentyp nicht width, sondern int.
// Lektor: UML toString() war nicht gefordert
public class Main {
  public static void main(String[] args) {
    // Erzeugen einer Instanz der Businesslogik
    Controller ctrl = new Controller();
    // Starten der Businesslogik
    ctrl.create();
  }
}