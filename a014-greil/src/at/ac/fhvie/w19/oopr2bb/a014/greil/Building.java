package at.ac.fhvie.w19.oopr2bb.a014.greil;

import java.util.StringJoiner;

@SuppressWarnings("checkstyle:LineLength")
class Building {

  private static final int DEFAULT_ELEVATOR_COUNT = 0;
  private static final int DEFAULT_FLOOR_COUNT = 1;
  private static final String EX_MESSAGE_ADDRESS = "address must not be empty. \n";
  private static final String EX_MESSAGE_ELEVATOR_COUNT = "elevatorCount have to be higher then 0, actual value: ";
  private static final String EX_MESSAGE_FLOOR_COUNT = "floorCount have to be higher then 0, actual value: ";

  private String address;
  private int floorCount;
  private int elevatorCount;

  Building(final String address) {
    this(address, DEFAULT_ELEVATOR_COUNT, DEFAULT_FLOOR_COUNT);
  }

  Building(final String address, final int elevatorCount) {
    this(address, elevatorCount, DEFAULT_FLOOR_COUNT);
  }

  Building(final String address, final int elevatorCount, final int floorCount) {
    boolean error = !(isValidAddress(address) && isValidElevatorCount(floorCount) && isValidFloorCount(elevatorCount));

    if (error) {
      String errorMessage = "";
      if (!isValidAddress(address)) {
        errorMessage = getAddressExceptionMessage();
      }
      if (!isValidElevatorCount(floorCount)) {
        errorMessage += getElevatorCountExceptionMessage(elevatorCount);
      }
      if (!isValidFloorCount(elevatorCount)) {
        errorMessage += getFloorCountExceptionMessage(floorCount);
      }

      throw new IllegalArgumentException(errorMessage);
    }

    this.address = address;
    this.floorCount = floorCount;
    this.elevatorCount = elevatorCount;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(final String address) {
    if (!isValidAddress(address)) {
      throw new IllegalArgumentException(getAddressExceptionMessage());
    }
    this.address = address;
  }

  public int getFloorCount() {
    return floorCount;
  }

  public void setFloorCount(final int floorCount) {
    if (!isValidFloorCount(floorCount)) {
      throw new IllegalArgumentException(getFloorCountExceptionMessage(floorCount));
    }
    this.floorCount = floorCount;
  }

  public int getElevatorCount() {
    return elevatorCount;
  }

  public void setElevatorCount(final int elevatorCount) {
    if (!isValidElevatorCount(elevatorCount)) {
      throw new IllegalArgumentException(getElevatorCountExceptionMessage(elevatorCount));
    }
    this.elevatorCount = elevatorCount;
  }

  private String getAddressExceptionMessage() {
    return EX_MESSAGE_ADDRESS;
  }

  private String getFloorCountExceptionMessage(final int value) {
    return EX_MESSAGE_FLOOR_COUNT + value + "\n";
  }

  private String getElevatorCountExceptionMessage(final int value) {
    return EX_MESSAGE_ELEVATOR_COUNT + value + "\n";
  }

  private boolean isValidAddress(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidFloorCount(final int value) {
    return (value >= 0);
  }

  private boolean isValidElevatorCount(final int value) {
    return (value >= 0);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Building.class.getSimpleName() + "[", "]")
        .add("address='" + address + "'")
        .add("floorCount=" + floorCount)
        .add("elevatorCount=" + elevatorCount)
        .toString();
  }
}

/*
@startuml
class Building{
- {static} EX_MESSAGE_ADDRESS : String = "address must not be empty. "
- {static} EX_MESSAGE_ELEVATOR_COUNT : String = "elevatorCount have to be higher then 0, actual value: "
- {static} EX_MESSAGE_FLOOR_COUNT : String = "floorCount have to be higher then 0, actual value: "
- {static} DEFAULT_ELEVATOR_COUNT : int = 0;
- {static} DEFAULT_FLOOR_COUNT : int = 1;
- floorCount: int {>=0}
- elevatorCount: int {>=0}
- address : String  {notNull, notEmpty}
--
+ Building(address : String)
+ Building(address : String, elevatorCount : int)
+ Building(address : String, elevatorCount : int, floorCount : int)
--
+ getAdress() : String
+ setAdress(address : String)
+ getFloorCount() : int
+ setFloorCount(floorCount: int)
+ setElevatorCount() : int
+ getElevatorCount(elevatorCount: int)
+ toString()
--
- isValidAddress(String value) : boolean
- getAddressExceptionMessage() : String
- isValidFloorCount(int value) : boolean
- getFloorCountExceptionMessage(int value) : String
- isValidElevatorCount(int value) : boolean
- getElevatorCountExceptionMessage(int value) : String
}
@enduml
 */
