package at.ac.fhvie.w19.oopr2bb.a020.greil;

class Controller {

  private static final int PORT1 = 4;
  private static final int PORT2 = 5;
  private static final int PORT3 = 5;
  private static final String OS = "Chrome OS";
  private static final String OS2 = "OSX";
  private static final double FREQUENCE = 3.4;

  private static final int ILLEGAL_VALUE_PORT = -3;
  private static final String ILLEGAL_VALUE_OS = "";
  private static final double ILLEGAL_VALUE_FREQUENCE = -2.0;

  void create() {
    Computer computer1 = new Computer(PORT1);
    Computer computer2 = new Computer(PORT2, OS);
    Computer computer3 = new Computer(PORT3, OS2, FREQUENCE);
    System.out.println(computer1);
    System.out.println(computer2);
    System.out.println(computer3);

    try {
      new Computer(ILLEGAL_VALUE_PORT, ILLEGAL_VALUE_OS, ILLEGAL_VALUE_FREQUENCE);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
      System.out.println("Exit");
      System.exit(1);
    }
  }
}

