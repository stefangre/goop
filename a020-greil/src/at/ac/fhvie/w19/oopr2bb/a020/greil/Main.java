package at.ac.fhvie.w19.oopr2bb.a020.greil;

public class Main {
  public static void main(String[] args) {
    // Erzeugen einer Instanz der Businesslogik
    Controller ctrl = new Controller();
    // Starten der Businesslogik
    ctrl.create();
  }
}