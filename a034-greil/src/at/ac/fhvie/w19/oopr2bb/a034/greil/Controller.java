package at.ac.fhvie.w19.oopr2bb.a034.greil;

class Controller {

  private static final int FLOOR_COUNT = 1;
  private static final int FLOOR_COUNT2 = 2;
  private static final int FLOOR_COUNT3 = 3;
  private static final int ELEVATOR_COUNT = 1;
  private static final int ELEVATOR_COUNT2 = 2;

  private static final int ILLEGAL_VALUE_FLOOR_COUNT = -10;
  private static final int ILLEGAL_VALUE_ELEVATOR_COUNT = -2;

  void create() {
    Building building1 = new Building(FLOOR_COUNT);
    Building building2 = new Building(FLOOR_COUNT2, ELEVATOR_COUNT);
    Building building3 = new Building(FLOOR_COUNT3, ELEVATOR_COUNT2, false);
    System.out.println(building1);
    System.out.println(building2);
    System.out.println(building3);

    try {
      new Building(ILLEGAL_VALUE_FLOOR_COUNT, ILLEGAL_VALUE_ELEVATOR_COUNT, true);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
      System.out.println("Exit");
      System.exit(1);
    }
  }
}
