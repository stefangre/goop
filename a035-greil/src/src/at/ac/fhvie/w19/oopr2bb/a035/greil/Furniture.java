package src.at.ac.fhvie.w19.oopr2bb.a035.greil;

import java.util.StringJoiner;

@SuppressWarnings("checkstyle:LineLength")
public class Furniture {

  private static final int DEFAULT_WEIGHT = 1;
  private static final String DEFAULT_COLOR = "White";
  private static final String EX_MESSAGE_MODEL = "Model must not be empty. \n";
  private static final String EX_MESSAGE_COLOR = "Color must not be empty. \n";
  private static final String EX_MESSAGE_WEIGHT = "Weight have to be higher then 0, actual value: ";

  private String model;
  private String color;
  private int weightInKg;

  Furniture(final String model) {
    this(model, DEFAULT_COLOR, DEFAULT_WEIGHT);
  }

  Furniture(final String model, final String color) {
    this(model, color, DEFAULT_WEIGHT);
  }

  Furniture(final String model, final String color, final int weightInKg) {
    boolean error = !(isValidModel(model) && isValidColor(color) && isValidWeight(weightInKg));

    if (error) {
      String errorMessage = "";
      if (!isValidModel(model)) {
        errorMessage = getModelExceptionMessage();
      }
      if (!isValidColor(color)) {
        errorMessage += getColorExceptionMessage();
      }
      if (!isValidWeight(weightInKg)) {
        errorMessage += getWeightExceptionMessage(weightInKg);
      }
      throw new IllegalArgumentException(errorMessage);
    }

    this.model = model;
    this.color = color;
    this.weightInKg = weightInKg;
  }

  public String getModel() {
    return model;
  }

  public void setModel(final String model) {
    if (!isValidModel(model)) {
      throw new IllegalArgumentException(getModelExceptionMessage());
    }
    this.model = model;
  }

  public String getColor() {
    return color;
  }

  public void setColor(final String color) {
    if (!isValidColor(color)) {
      throw new IllegalArgumentException(getColorExceptionMessage());
    }
    this.color = color;
  }

  public int getWeightInKg() {
    return weightInKg;
  }

  public void setWeightInKg(final int weightInKg) {
    if (!isValidWeight(weightInKg)) {
      throw new IllegalArgumentException(getWeightExceptionMessage(weightInKg));
    }
    this.weightInKg = weightInKg;
  }

  private boolean isValidModel(final String value) {
    //https://stackoverflow.com/questions/3321526/should-i-use-string-isempty-or-equalsstring
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidColor(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidWeight(final int value) {
    return (value > 0);
  }

  private String getModelExceptionMessage() {
    return EX_MESSAGE_MODEL;
  }

  private String getColorExceptionMessage() {
    return EX_MESSAGE_COLOR;
  }

  private String getWeightExceptionMessage(final int value) {
    return EX_MESSAGE_WEIGHT + value + "\n";
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Furniture.class.getSimpleName() + "[", "]")
        .add("model: '" + model + "'")
        .add("color: '" + color + "'")
        .add("weight: " + weightInKg)
        .toString();
  }
}

/*
@startuml
class Furniture{
- {static} EX_MESSAGE_MODEL : String = "Model must not be empty"
- {static} EX_MESSAGE_COLOR : String = "Color must not be empty"
- {static} EX_MESSAGE_WEIGHT : String = "Weight have to be higher then 0, actual value: "
- {static} DEFAULT_WEIGHT : int = 1;
- {static} DEFAULT_COLOR : String = "white";
- model: String {notNull, notEmpty}
- color: String {notNull, notEmpty}
- weight : int {>0}
--
+ Furniture(model : String)
+ Furniture(model : String, color : String)
+ Furniture(model : String, color : String, weight : int)
--
+ getModel() : String
+ setModel(model: String)
+ setColor() : String
+ getColor(color: String)
+ setWeight() : int
+ isWeight(weight : int)
+ toString()
--
- isValidModel(value: String) : boolean
- getModelExceptionMessage() : String
- isValidColor(value: String) : boolean
- getColorExceptionMessage() : String
- isValidWeight(value: int) : boolean
- getColorExceptionMessage(value: int) : String
}
@enduml
 */