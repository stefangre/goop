package at.ac.fhvie.w19.oopr2bb.a040.greil;

import java.util.StringJoiner;

@SuppressWarnings("checkstyle:LineLength")
public class Computer {

  private static final double DEFAULT_FREQUENCY = 1.0;
  private static final String DEFAULT_OS = "Linux";
  private static final String EX_MESSAGE_MANUFRACTURER = "manufacturer must not be empty. \n";
  private static final String EX_MESSAGE_OS = "operatingSystem must not be empty. \n";
  private static final String EX_MESSAGE_FREQUENCY = "frequency have to be higher then 0.0, actual value: ";

  private String manufacturer;
  private String operatingSystem;
  private double frequency;

  Computer(final String manufacturer) {
    this(manufacturer, DEFAULT_OS, DEFAULT_FREQUENCY);
  }

  Computer(final String manufacturer, final String operatingSystem) {
    this(manufacturer, operatingSystem, DEFAULT_FREQUENCY);
  }

  Computer(final String manufacturer, final String operatingSystem, final double frequency) {
    boolean error = !(isValidManufacturer(manufacturer) && isValidOperatingSystem(operatingSystem) && isValidFrequency(frequency));

    if (error) {
      String errorMessage = "";
      if (!isValidOperatingSystem(manufacturer)) {
        errorMessage = getManufracturerExceptionMessage();
      }
      if (!isValidOperatingSystem(operatingSystem)) {
        errorMessage += getOperatingSystemExceptionMessage();
      }
      if (!isValidFrequency(frequency)) {
        errorMessage += getFrequencyExceptionMessage(frequency);
      }
      throw new IllegalArgumentException(errorMessage);
    }

    this.manufacturer = manufacturer;
    this.operatingSystem = operatingSystem;
    this.frequency = frequency;
  }

  public String getManufacturer() {
    return manufacturer;
  }

  public void setManufacturer(final String manufacturer) {
    if (!isValidManufacturer(manufacturer)) {
      throw new IllegalArgumentException(getManufracturerExceptionMessage());
    }
    this.manufacturer = manufacturer;
  }

  public String getOperatingSystem() {
    return operatingSystem;
  }

  public void setOperatingSystem(final String operatingSystem) {
    if (!isValidOperatingSystem(operatingSystem)) {
      throw new IllegalArgumentException(getOperatingSystemExceptionMessage());
    }
    this.operatingSystem = operatingSystem;
  }

  public double getFrequency() {
    return frequency;
  }

  public void setFrequency(final double frequency) {
    if (!isValidFrequency(frequency)) {
      throw new IllegalArgumentException(getFrequencyExceptionMessage(frequency));
    }
    this.frequency = frequency;
  }

  private boolean isValidManufacturer(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidOperatingSystem(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidFrequency(final double value) {
    return value > 0;
  }

  private String getManufracturerExceptionMessage() {
    return EX_MESSAGE_MANUFRACTURER;
  }

  private String getOperatingSystemExceptionMessage() {
    return EX_MESSAGE_OS;
  }

  private String getFrequencyExceptionMessage(final double value) {
    return EX_MESSAGE_FREQUENCY + value + "\n";
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Computer.class.getSimpleName() + "[", "]")
        .add("manufacturer: '" + manufacturer + "'")
        .add("operatingSystem: '" + operatingSystem + "'")
        .add("frequency: " + frequency)
        .toString();
  }
}

/*
@startuml
class Computer{
- {static} DEFAULT_FREQUENCY : double = 1.0
- {static} DEFAULT_OS : String = "Linux"
- {static} EX_MESSAGE_MANUFRACTURER : String = "manufacturer must not be empty."
- {static} EX_MESSAGE_OS : String = "operatingSystem must not be empty."
- {static} EX_MESSAGE_FREQUENCY : String = "frequency have to be higher then 0, actual value: "
- manufacturer: String {notNull, notEmpty}
- operatingSystem: String {notNull, notEmpty}
- frequency : double {>0}
--
+ Computer(manufacturer : String)
+ Computer(manufacturer : String, operatingSystem : String)
+ Computer(manufacturer : String, operatingSystem : String, frequency : double)
--
+ getManufacturer() : String
+ setManufacturer(manufacturer: String)
+ setOperatingSystem() : String
+ getOperatingSystem(operatingSystem: String)
+ setFrequency() : double
+ getFrequency(frequency : double)
+ toString() : String
--
- isValidManufacturer(value: String) : boolean
- getManufacturerExceptionMessage() : String
- isValidOperatingSystem(value: String) : boolean
- getOperatingSystemExceptionMessage() : String
- isValidFrequency(value: double) : boolean
- getFrequencyExceptionMessage(value: double) : String
}
@enduml
 */