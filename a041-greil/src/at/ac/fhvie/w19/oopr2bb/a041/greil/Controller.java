package at.ac.fhvie.w19.oopr2bb.a041.greil;

class Controller {

  private static final String LASTNAME = "Einstein";
  private static final String LASTNAME2 = "Hawking";
  private static final String LASTNAME3 = "Newton";
  private static final String FIRSTNAME = "Stephen";
  private static final String FIRSTNAME2 = "Isaac";
  private static final int AGE = 84;

  private static final String ILLEGAL_VALUE_LASTNAME = "";
  private static final String ILLEGAL_VALUE_FIRSTNAME = "";
  private static final int ILLEGAL_VALUE_AGE = -20;

  void create() {
    Person person1 = new Person(LASTNAME);
    Person person2 = new Person(LASTNAME2, FIRSTNAME);
    Person person3 = new Person(LASTNAME3, FIRSTNAME2, AGE);
    System.out.println(person1);
    System.out.println(person2);
    System.out.println(person3);

    try {
      new Person(ILLEGAL_VALUE_LASTNAME, ILLEGAL_VALUE_FIRSTNAME, ILLEGAL_VALUE_AGE);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
      System.out.println("Exit");
      System.exit(1);
    }
  }
}
