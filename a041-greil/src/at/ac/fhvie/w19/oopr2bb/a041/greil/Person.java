package at.ac.fhvie.w19.oopr2bb.a041.greil;

import java.util.StringJoiner;

@SuppressWarnings("checkstyle:LineLength")
public class Person {

  private static final int DEFAULT_AGE = 18;
  private static final String DEFAULT_NAME = "John";
  private static final String EX_MESSAGE_LASTNAME = "lastName must not be empty. \n";
  private static final String EX_MESSAGE_FIRSTNAME = "firstName must not be empty. \n";
  private static final String EX_MESSAGE_AGE = "age have to be higher or equal 0, actual value: ";

  private String lastName;
  private String firstName;
  private int age;

  Person(final String lastName) {
    this(lastName, DEFAULT_NAME, DEFAULT_AGE);
  }

  Person(final String lastName, final String firstName) {
    this(lastName, firstName, DEFAULT_AGE);
  }

  Person(final String lastName, final String firstName, final int age) {
    boolean error = !(isValidLastname(lastName) && isValidFirstName(firstName) && isValidAge(age));

    if (error) {
      String errorMessage = "";
      if (!isValidLastname(lastName)) {
        errorMessage = getLastnameExceptionMessage();
      }
      if (!isValidFirstName(firstName)) {
        errorMessage += getFirstNameExceptionMessage();
      }
      if (!isValidAge(age)) {
        errorMessage += getAgeExceptionMessage(age);
      }
      throw new IllegalArgumentException(errorMessage);
    }

    this.lastName = lastName;
    this.firstName = firstName;
    this.age = age;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    if (!isValidLastname(lastName)) {
      throw new IllegalArgumentException(getLastnameExceptionMessage());
    }
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    if (!isValidFirstName(firstName)) {
      throw new IllegalArgumentException(getFirstNameExceptionMessage());
    }
    this.firstName = firstName;
  }

  public int getAge() {
    return age;
  }

  public void setAge(final int age) {
    if (!isValidAge(age)) {
      throw new IllegalArgumentException(getAgeExceptionMessage(age));
    }
    this.age = age;
  }

  private boolean isValidLastname(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidFirstName(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidAge(final int value) {
    return value >= 0;
  }

  private String getLastnameExceptionMessage() {
    return EX_MESSAGE_LASTNAME;
  }

  private String getFirstNameExceptionMessage() {
    return EX_MESSAGE_FIRSTNAME;
  }

  private String getAgeExceptionMessage(final int value) {
    return EX_MESSAGE_AGE + value + "\n";
  }

  @Override
  public String toString() {
    //• [type=love,smell=turpentine,look=india ink]
    return new StringJoiner(", ", "• [", "]")
        .add("lastName=" + lastName)
        .add("firstName=" + firstName)
        .add("age=" + age)
        .toString();
  }
}

/*
@startuml
class Person{
- {static} DEFAULT_AGE : int = 1
- {static} DEFAULT_NAME : String = "John"
- {static} EX_MESSAGE_LASTNAME : String = "lastName must not be empty."
- {static} EX_MESSAGE_FIRSTNAME : String = "firstName must not be empty."
- {static} EX_MESSAGE_AGE : String = "age have to be higher or equal 0, actual value: "
- lastName: String {notNull, notEmpty}
- firstName: String {notNull, notEmpty}
- age : int {>0}
--
+ Person(lastName : String)
+ Person(lastName : String, firstName : String)
+ Person(lastName : String, firstName : String, age : int)
--
+ getLastname() : String
+ setLastname(lastName: String)
+ setFirstName() : String
+ getFirstName(firstName: String)
+ setAge() : int
+ getAge(age : int)
+ toString() : String
--
- isValidLastname(value: String) : boolean
- getLastnameExceptionMessage() : String
- isValidFirstName(value: String) : boolean
- getFirstNameExceptionMessage() : String
- isValidAge(value: String) : boolean
- getAgeExceptionMessage(value: String) : String
}
@enduml
 */