package at.ac.fhvie.w19.oopr2bb.a043.greil;

import java.util.StringJoiner;

@SuppressWarnings("checkstyle:LineLength")
public class Apartment {

  private static final int DEFAULT_ROOM_COUNT = 1;
  private static final int DEFAULT_WINDOW_COUNT = 1;
  private static final String EX_MESSAGE_AREA = "area have to be higher then 0, actual value: ";
  private static final String EX_ROOM_COUNT = "roomCount have to be higher then 0, actual value: ";
  private static final String EX_WINDOW_COUNT = "windowCount have to be higher then 0, actual value: ";

  private double area;
  private int roomCount;
  private int windowCount;

  Apartment(final double area) {
    this(area, DEFAULT_ROOM_COUNT, DEFAULT_WINDOW_COUNT);
  }

  Apartment(final double area, final int roomCount) {
    this(area, roomCount, DEFAULT_ROOM_COUNT);
  }

  Apartment(final double area, final int roomCount, final int windowCount) {
    boolean error = !(isValidArea(area) && isValidRoomCount(roomCount) && isValidWindowCount(windowCount));

    if (error) {
      String errorMessage = "";
      if (!isValidArea(area)) {
        errorMessage = getAreaExceptionMessage(area);
      }
      if (!isValidRoomCount(roomCount)) {
        errorMessage += getRoomCountExceptionMessage(roomCount);
      }
      if (!isValidRoomCount(windowCount)) {
        errorMessage += getWindowCountExceptionMessage(windowCount);
      }
      throw new IllegalArgumentException(errorMessage);
    }

    this.area = area;
    this.roomCount = roomCount;
    this.windowCount = windowCount;
  }

  public double getArea() {
    return area;
  }

  public void setArea(final double area) {
    if (!isValidArea(area)) {
      throw new IllegalArgumentException(getAreaExceptionMessage(area));
    }
    this.area = area;
  }

  public int getRoomCount() {
    return roomCount;
  }

  public void setRoomCount(final int roomCount) {
    if (!isValidRoomCount(roomCount)) {
      throw new IllegalArgumentException(getRoomCountExceptionMessage(roomCount));
    }
    this.roomCount = roomCount;
  }

  public int getWindowCount() {
    return windowCount;
  }

  public void setWindowCount(final int windowCount) {
    if (!isValidWindowCount(windowCount)) {
      throw new IllegalArgumentException(getWindowCountExceptionMessage(windowCount));
    }
    this.windowCount = windowCount;
  }

  private boolean isValidArea(final double value) {
    return value > 0;
  }

  private boolean isValidRoomCount(final int value) {
    return value > 0;
  }

  private boolean isValidWindowCount(final int value) {
    return value > 0;
  }

  private String getAreaExceptionMessage(final double value) {
    return EX_MESSAGE_AREA + value + "\n";
  }

  private String getRoomCountExceptionMessage(final int value) {
    return EX_ROOM_COUNT + value + "\n";
  }

  private String getWindowCountExceptionMessage(final int value) {
    return EX_WINDOW_COUNT + value + "\n";
  }

  @Override
  public String toString() {
    //• {type = “love“, smell = “turpentine“, look = “india ink“}
    return new StringJoiner(", ", "• {", "}")
        .add("area = \"" + area + "\"")
        .add("roomCount = \"" + roomCount + "\"")
        .add("windowCount = \"" + windowCount + "\"")
        .toString();
  }
}

/*
@startuml
class Apartment{
- {static} DEFAULT_ROOM_COUNT : int = 1
- {static} DEFAULT_WINDOW_COUNT : int = 1
- {static} EX_MESSAGE_AREA : String = "area have to be higher then 0, actual value: "
- {static} EX_ROOM_COUNT : String = "roomCount have to be higher then 0, actual value: "
- {static} EX_WINDOW_COUNT : String = "windowCount have to be higher then 0, actual value:  "
- area: double {>0}
- roomCount: int {>0}
- windowCount : int {>0}
--
+ Apartment(area : double)
+ Apartment(area : double, roomCount : int)
+ Apartment(area : double, roomCount : int, windowCount : int)
--
+ getArea() : double
+ setArea(lastName: double)
+ setRoomCount() : int
+ getRoomCount(roomCount: int)
+ setWindowCount() : int
+ getWindowCount(windowCount : int)
+ toString() : String
--
- isValidArea(value : double) : boolean
- getAreaExceptionMessage(value : double) : String
- isValidRoomCount(value : int) : boolean
- getRoomCountExceptionMessage(value : int) : String
- isValidWindowCount(value : int) : boolean
- getWindowCountExceptionMessage(value : int) : String
}
@enduml
 */