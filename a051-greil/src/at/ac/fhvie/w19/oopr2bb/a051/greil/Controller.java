package at.ac.fhvie.w19.oopr2bb.a051.greil;

import java.util.ArrayList;
import java.util.Arrays;

class Controller {

  private static final boolean CONFORTABLE = true;
  private static final boolean NOT_CONFORTABLE = false;

  void create() {
    Chair armchair = new Chair(CONFORTABLE);
    Chair cantileverChair = new Chair(CONFORTABLE);
    Chair wingChair = new Chair(NOT_CONFORTABLE);
    Chair deckChair = new Chair(CONFORTABLE);
    Chair deskChair = new Chair(NOT_CONFORTABLE);

    //add all Instances to the ArrayList
    ArrayList<Chair> chairs = new ArrayList<>(Arrays.asList(armchair, cantileverChair, wingChair, deckChair, deskChair));

    //Iterate trough the List and print out the objects which where comfortable
    for (Chair chair : chairs) {
      if(chair.isComfortable()){
        System.out.println(chair);
      }
    }
  }
}
