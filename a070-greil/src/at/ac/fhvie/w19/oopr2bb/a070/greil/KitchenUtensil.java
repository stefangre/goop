package at.ac.fhvie.w19.oopr2bb.a070.greil;

import java.util.StringJoiner;

/*
Erstellen Sie eine Klasse, welche ein Kückenutensil beschreibt, welches nützlich ist oder nicht
--> Ich gehe mal davon aus das es das "KücHenutensil" sein soll :)
 */

public class KitchenUtensil {

  private final boolean useful;

  KitchenUtensil(final boolean useful) {
    this.useful = useful;
  }

  public boolean isUseful() {
    return useful;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", KitchenUtensil.class.getSimpleName() + "[", "]")
        .add("ist \"" + (useful ? "nützlich" : "nicht nützlich") + "\"")
        .toString();
  }
}

/*
@startuml
class KitchenUtensil{
- useful: boolean
--
+ Chair(useful : boolean)
--
+ isUseful() : boolean
+ toString() : String

}
@enduml
 */