package at.ac.fhvie.w19.oopr2bb.a071.greil;

import java.util.StringJoiner;

public class Chair {
  private final boolean comfortable;

  Chair(final boolean comfortable) {
    this.comfortable = comfortable;
  }

  public boolean isComfortable() {
    return comfortable;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Chair.class.getSimpleName() + "[", "]")
        .add("ist \"" + (comfortable ? "bequem" : "nicht bequem") + "\"")
        .toString();
  }
}

/*
@startuml
class Chair{
- comfortable: boolean
--
+ Chair(comfortable : boolean)
--
+ isComfortable() : boolean
+ toString() : String

}
@enduml
 */