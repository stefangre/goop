package at.ac.fhvie.w19.oopr2bb.a071.greil;

public class Main {
  private static void main(final String[] args) {
    // Erzeugen einer Instanz der Businesslogik
    Controller ctrl = new Controller();
    // Starten der Businesslogik
    ctrl.create();
  }
}
