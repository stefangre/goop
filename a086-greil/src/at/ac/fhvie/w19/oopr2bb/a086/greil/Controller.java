package at.ac.fhvie.w19.oopr2bb.a086.greil;

class Controller {

  void create() {
    Merchandise shirt = new Merchandise();
    Merchandise trousers = new Merchandise();
    Merchandise cap = new Merchandise();

    System.out.println(shirt.getId());
    System.out.println(trousers.getId());
    System.out.println(cap.getId());

  }
}
