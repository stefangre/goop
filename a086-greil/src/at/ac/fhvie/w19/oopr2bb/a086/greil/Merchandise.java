package at.ac.fhvie.w19.oopr2bb.a086.greil;

public class Merchandise {

  private static final int START_COUNTER = 1000;
  private static int idCounter = START_COUNTER;
  private int id;

  public Merchandise() {
    this.id = idCounter++;
  }

  public int getId() {
    return id;
  }
}

/*
@startuml
class Merchandise{
- {static} START_COUNTER : int = 1000
- {static} idCounter : int = 1000
- id : int
--
+ Merchandise()
--
+ getId() : int
}
@enduml
 */
