package at.ac.fhvie.w19.oopr2bb.a110.greil;

public class Bedclothes {
  private static int idCounter = 0;
  private int id;
  private Material material;

  public Bedclothes(final Material material) {
    this.material = material;
    this.id = idCounter++;
  }

  public int getId() {
    return id;
  }

  public Material getMaterial() {
    return material;
  }

  public void setMaterial(final Material material) {
    this.material = material;
  }

  @Override
  public String toString() {
    return "Bettwäsche: " + material;
  }
}

/*
@startuml
class Bedclothes{
- {static} idCounter : int = 0
- id : int
- material : Material
--
+ Bedclothes(material: Material)
--
+ getId() : int
+ setMaterial(material : Material)
+ getMaterial() : Material
+ toString() : String
}

Bedclothes ..> Material : <<use>>

enum Material{
  SATIN(String description)
  LEINEN(String description)
  STOFF(String description)
- description : String
- priceClass : String
--
+ Material(String description, String PriceClass)
--
+ getDescription() : String
+ getPriceClass() : String
+ toString() : String
}
@enduml
 */