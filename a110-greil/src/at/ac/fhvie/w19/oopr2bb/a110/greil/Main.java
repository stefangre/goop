package at.ac.fhvie.w19.oopr2bb.a110.greil;

final class Main {

  private Main() {
  }

  public static void main(final String[] args) {
    Controller ctrl = new Controller();
    ctrl.create();
  }
}
