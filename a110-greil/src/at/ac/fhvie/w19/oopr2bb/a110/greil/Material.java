package at.ac.fhvie.w19.oopr2bb.a110.greil;

public enum Material {
  SATIN("Satin", "E4"),
  LEINEN("Leinen", "E6"),
  STOFF("Stoff", "E12");

  private String description;
  private String priceClass;

  Material(final String description, final String priceClass) {
    this.description = description;
    this.priceClass = priceClass;
  }

  public String getDescription() {
    return description;
  }

  public String getPriceClass() {
    return priceClass;
  }

  public String toString() {
    return "Beschreibung: " + description + ", Preisklasse: " + priceClass;
  }
}
/*
@startuml
enum Material{
  SATIN(String description)
  LEINEN(String description)
  STOFF(String description)
- description : String
- priceClass : String
--
+ Material(String description, String PriceClass)
--
+ getDescription() : String
+ getPriceClass() : String
+ toString() : String
}
@enduml
 */