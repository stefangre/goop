package at.ac.fhvie.w19.oopr2bb.a110.greil;

public enum Alignment {
  KUST,
  NATURWISSENSCHAFT,
  GESCHICHTE
}

/*
@startuml
enum Alignment{
  KUST
  NATURWISSENSCHAFT
  GESCHICHTE
}
@enduml
 */