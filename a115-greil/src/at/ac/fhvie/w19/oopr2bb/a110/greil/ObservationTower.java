package at.ac.fhvie.w19.oopr2bb.a110.greil;

import java.util.StringJoiner;

public class ObservationTower extends Attraction {
  private static final String EX_MESSAGE_DEFAULT_HEIGHT =
      "The height have to be higher than 0";
  private static final int DEFAULT_HEIGHT = 100;
  private int heightInMeters;

  public ObservationTower(final String name, final int lastYearVisitorCount) {
    this(name, lastYearVisitorCount, DEFAULT_HEIGHT);
  }

  public ObservationTower(final String name, final int lastYearVisitorCount,
                          final int heightInMeters) {
    super(name, lastYearVisitorCount);
    if (!super.isValidPosNumber(heightInMeters)) {
      throw new IllegalArgumentException(EX_MESSAGE_DEFAULT_HEIGHT);
    }
    this.heightInMeters = heightInMeters;
  }

  public int getHeightInMeters() {
    return heightInMeters;
  }

  public void setHeightInMeters(final int heightInMeters) {
    this.heightInMeters = heightInMeters;
  }

  @Override
  public String toString() {
    //[type=love,smell=turpentine,look=india ink]
    return new StringJoiner(", ", "[", "]")
        .add("globalId=" + getGlobalId())
        .add("heightInMeters=" + heightInMeters)
        .toString();
  }
}

/*
@startuml
class ObservationTower extends Attraction {
- {static} EX_MESSAGE_DEFAULT_HEIGHT : String = "The height have to be higher than 0"
- {static} DEFAULT_HEIGHT : int = 100
- heightInMeters : int
--
+ ObservationTower(name: String, lastYearVisitorCount int)
+ ObservationTower(name: String, lastYearVisitorCount: int, heightInMeters : int)
--
+ getHeightInMeters() : int
+ setHeightInMeters(heightInMeters : int)
+ toString() : String
}
@enduml
 */
