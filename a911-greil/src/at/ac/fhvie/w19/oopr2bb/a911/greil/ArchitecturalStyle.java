package at.ac.fhvie.w19.oopr2bb.a911.greil;

import java.util.StringJoiner;

public enum ArchitecturalStyle {

  ROMANTIK("Romantik", "(1000–1250)"),
  GOTIK("Gotik", " (1250–1520)"),
  RENAISSANCE("Renaissance", " (1510–1620)"),
  BAROCK("Barock", "(1600–1770)"),
  KLSSIZISMUS("Klassizismus", "(1770–1830)");

  private String name;
  private String timeDuration;

  ArchitecturalStyle(final String name, final String timeDuration) {
    this.name = name;
    this.timeDuration = timeDuration;
  }

  public String getName() {
    return name;
  }

  public String getTimeDuration() {
    return timeDuration;
  }


  @Override
  public String toString() {
    return new StringJoiner("")
        .add(name + " ")
        .add(timeDuration)
        .toString();
  }
}
