package at.ac.fhvie.w19.oopr2bb.a911.greil;

import java.util.StringJoiner;

public class Attraction {

  private static final double DEFAULT_ENTERENCE_FEE = 1.0;
  private static final ArchitecturalStyle DEFAULT_A_STYLE = ArchitecturalStyle.ROMANTIK;
  private String name;
  private ArchitecturalStyle architecturalStyle;

  private double entranceFee;

  public Attraction(final String name, final City city) {
    this(name, city, DEFAULT_ENTERENCE_FEE);
  }

  public Attraction(final String name, final City city, final double entranceFee) {
    this(name, city, entranceFee, DEFAULT_A_STYLE);
  }

  public Attraction(final String name, final City city, final double entranceFee,
                    final ArchitecturalStyle architecturalStyle) {
    this.name = name;
    this.architecturalStyle = architecturalStyle;
    this.entranceFee = entranceFee;
  }

  public ArchitecturalStyle getArchitecturalStyle() {
    return architecturalStyle;
  }

  public String getName() {
    return name;
  }

  //und die einer Sehenswürdigkeit [Name Baustil]

  public double getEntranceFee() {
    return entranceFee;
  }

  @Override
  public String toString() {
    return new StringJoiner(" ", "[", "]")
        .add(name + "")
        .add(architecturalStyle + "")
        .toString();
  }
}
