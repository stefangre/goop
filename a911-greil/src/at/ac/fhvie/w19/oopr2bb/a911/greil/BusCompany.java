package at.ac.fhvie.w19.oopr2bb.a911.greil;

import java.util.ArrayList;

public final class BusCompany {
  private static final String EX_MESSAGE_TRIP_NULL = "roundTrip is not allow to be null";
  private static final String NEW_LINE = "\n";
  private static final String EX_MESSAGE_TRIPS_NULL = "roundTrips is not allow to be null";
  private static final String EX_MESSAGE_CITIES_NULL = "cities is not allow to be null";
  private static final String EX_MESSAGE_TRIP_START_CITY = "it is not allow to delete a city from a"
      + " trip, when the name of this city is equal to the name of the roudtrips startcity. ID: ";
  private static ArrayList<RoundTrip> roundTrips = new ArrayList<RoundTrip>();

  public static ArrayList<RoundTrip> getRoundTrips() {
    return roundTrips;
  }

  public static void addRoundTrip(final RoundTrip roundTrip) {
    if (Utility.objectNotNull(roundTrip)) {
      roundTrips.add(roundTrip);
    } else {
      throw new IllegalArgumentException(EX_MESSAGE_TRIP_NULL);
    }
  }

  public static int entfernen(final String name) {
    int count = 0;
    String error = "";
    if (Utility.objectNotNull(roundTrips)) {
      for (RoundTrip roundTrip : roundTrips) {
        if (!(roundTrip.getStartCity().getName() == name)) {
          ArrayList<City> cities = roundTrip.getCities();
          if (Utility.objectNotNull(cities)) {
            for (City city : cities) {
              if (city.getName() == name) {
                count++;
                cities.remove(city);
                break;
              }
            }
          } else {
            throw new IllegalArgumentException(EX_MESSAGE_CITIES_NULL);
          }
        } else {
          error += EX_MESSAGE_TRIP_START_CITY + roundTrip.getId() + NEW_LINE;
        }
      }
    } else {
      throw new IllegalArgumentException(EX_MESSAGE_TRIPS_NULL);
    }

    if (Utility.stringNotNullOrEmpty(error)) {
      throw new IllegalArgumentException(error);
    }

    return count;
  }
}
