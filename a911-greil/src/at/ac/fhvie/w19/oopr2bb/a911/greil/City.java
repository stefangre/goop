package at.ac.fhvie.w19.oopr2bb.a911.greil;

import java.util.ArrayList;
import java.util.StringJoiner;

public class City {

  private String name;
  private ArrayList<Attraction> attractions;

  public City(final String name) {
    this(name, new ArrayList<>());
  }

  public City(final String name, final ArrayList<Attraction> attractions) {
    this.name = name;
    this.attractions = new ArrayList<>(attractions);
  }

  public String getName() {
    return name;
  }

  public String printAttractions(final ArrayList<Attraction> attractionsToPrint) {
    String returnvalue = "";
    for (Attraction attraction : attractionsToPrint) {
      returnvalue += attraction.getName() + " ";
    }
    return returnvalue;
  }

  public ArrayList<Attraction> getAttractions() {
    return attractions;
  }

  public void setAttractions(final ArrayList<Attraction> attractions) {
    this.attractions = attractions;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add(name + "")
        .add("[" + printAttractions(attractions) + "]")
        .toString();
  }
}
