package at.ac.fhvie.w19.oopr2bb.a911.greil;

import java.util.ArrayList;
import java.util.Arrays;

class Controller {
  private static final String A_DOM_NAME = "Salzburger Dom";
  private static final String A_NYM_NAME = "Schloss Nymphenburg";
  private static final String A_RRD_NAME = "Riesenrad";
  private static final String A_SD_NAME = "Stephansdom";
  private static final String A_THE_NAME = "Theresienwiese";
  private static final String A_RAT_NAME = "Rathaus";
  private static final String A_KIR_NAME = "Kirtag";
  private static final String A_OPER_NAME = "Oper";

  private static final String CITY_SALZBURG = "Salzburg";
  private static final String CITY_MUE = "München";
  private static final String CITY_WIEN = "Wien";
  private static final String CITY_PULKAU = "Pulkau";
  private static final String CITY_OSLO = "Oslo";
  private static final String CITY_HORN = "Horn";
  private static final String CITY_RETZ = "Retz";
  private static final String CITY_LAA = "Laa";
  private static final String CITY_BADEN = "Baden";
  private static final String CITY_HAAG = "Haag";
  private static final String CITY_MELK = "Melk";
  private static final String CITY_KILB = "Kilb";
  private static final String CITY_YBBS = "Ybbs";

  private static final int FEE_SD = 100;
  private static final int FEE_NB = 10;
  private static final int FEE_SDM = 0;
  private static final int FEE_RH = 100;
  private static final int FEE_RHH = 10;
  private static final int FEE_RR = 150;
  private static final int FEE_KT = 50;
  private static final int FEE_OR = 350;

  private static final int SEATCOUNT_RT_WIEN = 10;
  private static final int SEATCOUNT_RT_PULKAU = 100;
  private static final int SEATCOUNT_RT_HORN = 200;
  private static final int SEATCOUNT_RT_RETZ = 200;
  private static final int SEAT_COUNT_LD_WIEN = 70;

  private static final String EX_MESSAGE_CITIES_NULL = "List is null";
  private static final String EX_MESSAGE_AS_NULL = "architecturalStyle is null";

  void create() {
    City salzburg = new City(CITY_SALZBURG);
    City wien = new City(CITY_WIEN);
    City muenchen = new City(CITY_MUE);
    City pulkau = new City(CITY_PULKAU);
    City oslo = new City(CITY_OSLO);
    City horn = new City(CITY_HORN);
    City retz = new City(CITY_RETZ);
    City laa = new City(CITY_LAA);

    final Attraction salzburgerDom = new Attraction(A_DOM_NAME, salzburg, FEE_SD,
        ArchitecturalStyle.BAROCK);
    final Attraction nymphenburg = new Attraction(A_NYM_NAME, muenchen, FEE_NB,
        ArchitecturalStyle.BAROCK);
    final Attraction theresienwiese = new Attraction(A_THE_NAME, muenchen, FEE_NB,
        ArchitecturalStyle.BAROCK);
    Attraction oper = new Attraction(A_OPER_NAME, oslo, FEE_OR, ArchitecturalStyle.GOTIK);
    final Attraction rathausP = new Attraction(A_RAT_NAME, pulkau, FEE_RH,
        ArchitecturalStyle.GOTIK);
    final Attraction rathausH = new Attraction(A_RAT_NAME, horn, FEE_RHH,
        ArchitecturalStyle.RENAISSANCE);
    final Attraction rathausR = new Attraction(A_RAT_NAME, retz, FEE_RH,
        ArchitecturalStyle.ROMANTIK);
    final Attraction riesenrad = new Attraction(A_RRD_NAME, wien, FEE_RR,
        ArchitecturalStyle.KLSSIZISMUS);
    final Attraction stephansdom = new Attraction(A_SD_NAME, wien, FEE_SDM,
        ArchitecturalStyle.KLSSIZISMUS);
    final Attraction kirtagP = new Attraction(A_KIR_NAME, pulkau, FEE_KT,
        ArchitecturalStyle.KLSSIZISMUS);
    final Attraction kirtagL = new Attraction(A_KIR_NAME, laa, FEE_KT,
        ArchitecturalStyle.KLSSIZISMUS);

    ArrayList<Attraction> salzburgerAttractions = new ArrayList<Attraction>(
        Arrays.asList(salzburgerDom));
    salzburg.setAttractions(salzburgerAttractions);
    ArrayList<Attraction> muenchenAttractions = new ArrayList<Attraction>(
        Arrays.asList(nymphenburg, theresienwiese));
    muenchen.setAttractions(muenchenAttractions);
    ArrayList<Attraction> osloAttractions = new ArrayList<Attraction>(
        Arrays.asList(oper));
    oslo.setAttractions(osloAttractions);
    ArrayList<Attraction> pulkauAttractions = new ArrayList<Attraction>(
        Arrays.asList(rathausP, kirtagP));
    pulkau.setAttractions(pulkauAttractions);
    ArrayList<Attraction> hornAttractions = new ArrayList<Attraction>(Arrays.asList(rathausH));
    horn.setAttractions(hornAttractions);
    ArrayList<Attraction> retzAttractions = new ArrayList<Attraction>(Arrays.asList(rathausR));
    retz.setAttractions(retzAttractions);
    ArrayList<Attraction> wienAttractions = new ArrayList<Attraction>(Arrays.asList(riesenrad,
        stephansdom));
    wien.setAttractions(wienAttractions);
    ArrayList<Attraction> laaAttractions = new ArrayList<Attraction>(Arrays.asList(kirtagL));
    laa.setAttractions(laaAttractions);

    LongDistanceTrip longDistanceWienMuenchen = new LongDistanceTrip(SEAT_COUNT_LD_WIEN, wien,
        muenchen);

    System.out.println(longDistanceWienMuenchen);

    ArrayList citiesRtWien = new ArrayList<City>(Arrays.asList(salzburg, muenchen, horn, laa,
        pulkau));

    RoundTrip roundTripWien = new RoundTrip(SEATCOUNT_RT_WIEN, wien, citiesRtWien);

    //Print Rundreise [ID StartZielStadt Plätze [weitereStadt1 weitereStadt2 ...]]
    System.out.println(roundTripWien);

    //Print city [Name [Sehenswürdigkeit1 Sehenswürdigkeit2 ...]]
    System.out.println(wien);

    //Print Sehenswürdigkeit [Name Baustil]
    System.out.println(riesenrad);

    //Print LongDistanceTrip
    System.out.println(longDistanceWienMuenchen);

    //trigger "fee" - exception
    try {
      roundTripWien.addStadt(oslo);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
    }

    //trigger "allready in list" - exception
    try {
      roundTripWien.addStadt(pulkau);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
    }

    //trigger "more than 10 Cities" - exception
    City baden = new City(CITY_BADEN);
    City haag = new City(CITY_HAAG);
    City melk = new City(CITY_MELK);
    City kilb = new City(CITY_KILB);
    City ybbs = new City(CITY_YBBS);
    roundTripWien.addStadt(baden);
    roundTripWien.addStadt(haag);
    roundTripWien.addStadt(melk);
    roundTripWien.addStadt(kilb);
    try {
      roundTripWien.addStadt(ybbs);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
    }

    //trigger "RoundTrips not allowed then 10 Cities" - exception
    ArrayList citiesRtPulkauIllegalElevenCities = new ArrayList<City>(Arrays.asList(muenchen,
        baden, haag, melk, kilb, ybbs, salzburg, wien, retz, horn, ybbs));
    try {
      RoundTrip roundTripPulkau = new RoundTrip(SEATCOUNT_RT_WIEN, pulkau,
          citiesRtPulkauIllegalElevenCities);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
    }

    //trigger "RoundTrips needs min 2 Cities" - exception
    ArrayList citiesRtPulkauIllegalOneCity = new ArrayList<City>(Arrays.asList(muenchen));
    try {
      RoundTrip roundTripPulkau = new RoundTrip(SEATCOUNT_RT_WIEN, pulkau,
          citiesRtPulkauIllegalOneCity);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
    }

    //trigger "City in List" - exception
    ArrayList citiesRtPulkauIllegalCityInList = new ArrayList<City>(Arrays.asList(pulkau,
        baden, haag, melk, kilb, ybbs, salzburg));
    try {
      RoundTrip roundTripPulkau = new RoundTrip(SEATCOUNT_RT_WIEN, pulkau,
          citiesRtPulkauIllegalCityInList);
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
    }

    ArrayList expectedCities = new ArrayList<City>(Arrays.asList(salzburg, muenchen));
    ArrayList filteredCities = hatBaustil(citiesRtWien, ArchitecturalStyle.BAROCK);

    //print hatBaustil result
    System.out.println(filteredCities);
    boolean result = filteredCities.equals(expectedCities);
    System.out.println(result);

    ArrayList rtPulkauCities = new ArrayList<City>(Arrays.asList(kilb, ybbs, horn));
    RoundTrip roundTripPulkau = new RoundTrip(SEATCOUNT_RT_PULKAU, pulkau, rtPulkauCities);
    ArrayList rtRetzCities = new ArrayList<City>(Arrays.asList(laa, haag, baden));
    RoundTrip roundTripRetz = new RoundTrip(SEATCOUNT_RT_RETZ, retz, rtRetzCities);
    ArrayList rtHornCities = new ArrayList<City>(Arrays.asList(oslo, muenchen, ybbs));
    RoundTrip roundTripHorn = new RoundTrip(SEATCOUNT_RT_HORN, horn, rtHornCities);

    //list of RoundTrip Instances
    System.out.println(BusCompany.getRoundTrips());

    //Print entfernen()
    System.out.println(BusCompany.entfernen("Melk"));

    //trigger StartCity Error
    ArrayList rtHornCitiesSecond = new ArrayList<City>(Arrays.asList(wien, baden, ybbs));
    RoundTrip roundTripHornSecond = new RoundTrip(SEATCOUNT_RT_HORN, horn, rtHornCitiesSecond);
    try {
      System.out.println(BusCompany.entfernen("Horn"));
    } catch (IllegalArgumentException ex) {
      System.out.println(ex.getLocalizedMessage());
    }

    System.out.println(BusCompany.getRoundTrips());
  }

  private ArrayList<City> hatBaustil(final ArrayList<City> citiesToCheck,
                                     final ArchitecturalStyle architecturalStyle) {
    ArrayList<City> citiesWithSpecificArchitecturalStyle = new ArrayList<City>();

    if (!Utility.objectNotNull(architecturalStyle)) {
      throw new IllegalArgumentException(EX_MESSAGE_AS_NULL);
    }

    if (Utility.objectNotNull(citiesToCheck)) {
      for (City city : citiesToCheck) {
        ArrayList<Attraction> attractions = city.getAttractions();
        if (Utility.objectNotNull(attractions)) {
          for (Attraction attraction : attractions) {
            if (attraction.getArchitecturalStyle() == architecturalStyle) {
              citiesWithSpecificArchitecturalStyle.add(city);
              break;
            }
          }
        }
      }
    } else {
      throw new IllegalArgumentException(EX_MESSAGE_CITIES_NULL);
    }

    return citiesWithSpecificArchitecturalStyle;
  }

}
