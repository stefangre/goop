package at.ac.fhvie.w19.oopr2bb.a911.greil;

import java.util.Objects;
import java.util.StringJoiner;

public class LongDistanceTrip extends Trip {

  private City endCity;
  private static final char PREFIX = 'F';

  public LongDistanceTrip(final int seatCount, final City startCity, final City endCity) {
    super(seatCount, startCity, PREFIX);
    this.endCity = endCity;
  }

  public City getEndCity() {
    return endCity;
  }

  public void setEndCity(final City endCity) {
    this.endCity = endCity;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LongDistanceTrip that = (LongDistanceTrip) o;
    return Objects.equals(endCity, that.endCity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(endCity);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add(super.getId())
        .add(super.getStartCity().getName())
        .add(endCity.getName())
        .toString();
  }
}
