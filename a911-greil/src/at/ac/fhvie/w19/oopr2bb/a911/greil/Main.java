package at.ac.fhvie.w19.oopr2bb.a911.greil;

final class Main {

  private Main() {
  }

  public static void main(final String[] args) {
    // Erzeugen einer Instanz der Businesslogik
    Controller ctrl = new Controller();
    // Starten der Businesslogik
    ctrl.create();
  }
}
