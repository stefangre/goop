package at.ac.fhvie.w19.oopr2bb.a911.greil;

import java.util.ArrayList;
import java.util.StringJoiner;

public class RoundTrip extends Trip {
  private static final int MAX_VISITED_CITIES = 10;
  private static final int MIN_CITIES = 2;
  private static final double MAX_FEE = 500.0;
  private static final String EX_MESSAGE_ST_CITIES = "Cities is not allowed to be null";
  private static final String EX_MESSAGE_CONTAIN_CITIES = "Startcity is not allowed to be in "
      + "CityList";
  private static final String EX_MESSAGE_MAX_CITIES = "RoundTrips not allowed then 10 Cities in the"
      + " list";
  private static final String EX_MESSAGE_MIN_CITIES = "RoundTrips need minimum 2 Cities in the "
      + "list, otherwise they would be a Roudtrip";
  private static final String EX_MESSAGE_AS_LIST = "Cant add city, city allready in List";
  private static final String EX_MESSAGE_AS_MAX_CITIES = "Cant add city, more than 10 Cities";
  private static final String EX_MESSAGE_AS_FEE = "Cant add city, totalFee would be over 500€";
  private static final String EX_MESSAGE_AS_NULL = "Stadt is not allowed to be null";
  private static final char PREFIX = 'R';
  private ArrayList<City> cities;

  public RoundTrip(final int seatCount, final City startCity, final ArrayList<City> cities) {
    super(seatCount, startCity, PREFIX);

    String error = "";

    if (!Utility.objectNotNull(cities)) {
      error += EX_MESSAGE_ST_CITIES;
    }

    if (cities.size() < MIN_CITIES) {
      error += EX_MESSAGE_MIN_CITIES;
    }

    if (cities.size() > MAX_VISITED_CITIES) {
      error += EX_MESSAGE_MAX_CITIES;
    }

    if (cities.contains(startCity)) {
      error += EX_MESSAGE_CONTAIN_CITIES;
    }

    if (Utility.stringNotNullOrEmpty(error)) {
      throw new IllegalArgumentException(error);
    }

    this.cities = new ArrayList<>(cities);
    BusCompany.addRoundTrip(this);
  }

  public double gesamtEintrittsPreis() {
    double entireFee = 0.0;
    for (City city : cities) {
      entireFee += calculateCityFee(city);
    }
    return entireFee;
  }

  public ArrayList<City> getCities() {
    return cities;
  }

  private double calculateCityFee(final City cityToCalculate) {
    ArrayList<Attraction> attractions = new ArrayList<>(cityToCalculate.getAttractions());
    double cityFee = 0.0;

    for (Attraction attraction : attractions) {
      cityFee += attraction.getEntranceFee();
    }
    return cityFee;
  }

  public void addStadt(final City cityToAdd) {
    if (Utility.objectNotNull(cityToAdd)) {
      if (Utility.objectNotNull(cities)) {
        if (cities.contains(cityToAdd)) {
          throw new IllegalArgumentException(EX_MESSAGE_AS_LIST);
        }
        if (cities.size() >= MAX_VISITED_CITIES) {
          throw new IllegalArgumentException(EX_MESSAGE_AS_MAX_CITIES);
        }
        //falls der Gesamteintrittspreis (s.o.) auf der Reise 500 € übersteigen würde
        double tempFee = gesamtEintrittsPreis() + calculateCityFee(cityToAdd);
        if (tempFee > MAX_FEE) {
          throw new IllegalArgumentException(EX_MESSAGE_AS_FEE);
        }
      }
      cities.add(cityToAdd);
    } else {
      throw new IllegalArgumentException(EX_MESSAGE_AS_NULL);
    }
  }

  private String printCities(final ArrayList<City> citiesToPrint) {
    String returnvalue = "";

    if (!Utility.objectNotNull(cities)) {
      return returnvalue;
    }

    for (City city : cities) {
      returnvalue += city.getName() + " ";
    }
    return returnvalue;
  }

  @Override
  public String toString() {
    return new StringJoiner(" ", "[", "]")
        .add(super.getId())
        .add(super.getStartCity().getName() + "")
        .add(super.getSeatCount() + "")
        .add("[" + printCities(cities) + "]")
        .toString();
  }
}
