package at.ac.fhvie.w19.oopr2bb.a911.greil;

public abstract class Trip {

  private static final int DEFAULT_SEATCOUNT = 0;
  private static final String EX_MESSAGE_SEATCOUNT
      = "SeatCount have to be higher than 0, actual value: ";

  private static int idCounter = 0;
  private final String id;
  private int seatCount;
  private City startCity;
  private char prefix;

  public Trip(final int seatCount, final City startCity, final char prefix) {

    String errorMessage = "";
    if (!Utility.intNotZeroOrNegativ(seatCount)) {
      errorMessage += EX_MESSAGE_SEATCOUNT + seatCount;
    }
    if (!errorMessage.isEmpty()) {
      throw new IllegalArgumentException(errorMessage);
    }

    this.seatCount = seatCount;
    this.startCity = startCity;
    this.id = prefix + String.valueOf(idCounter++);
  }

  public String getId() {
    return id;
  }

  public int getSeatCount() {
    return seatCount;
  }

  public void setSeatCount(final int seatCount) {
    this.seatCount = seatCount;
  }

  public City getStartCity() {
    return startCity;
  }

  public void setStartCity(final City startCity) {
    this.startCity = startCity;
  }


}
