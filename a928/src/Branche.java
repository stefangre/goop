import java.util.StringJoiner;

public enum Branche {

  GEWERBE("Gewerbe", 12),
  HANDEL("Handel", 13),
  INDUSTRIE("Industrie", 14),
  MISCHFORM("Mischform", 15);

  private String gewerbe;
  private int kennzahlt;

  Branche(final String gewerbe, final int kennzahlt) {
    this.gewerbe = gewerbe;
    this.kennzahlt = kennzahlt;
  }

  public String getGewerbe() {
    return gewerbe;
  }

  public int getKennzahlt() {
    return kennzahlt;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Branche.class.getSimpleName() + "[", "]")
        .add("gewerbe='" + gewerbe + "'")
        .add("kennzahlt=" + kennzahlt)
        .toString();
  }
}


