import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;

class Controller {

  private static final String OSMAN = "Osman";
  private static final String STEFAN = "Stefan";
  private static final String RONALDO = "Ronaldo";
  private static final String MESSI = "Messi";
  private static final String FINANZAMT_NAME = "Landstraße";
  private static final Branche BRANCHE = Branche.INDUSTRIE;
  private static final Branche BRANCHE2 = Branche.GEWERBE;

  void create() {
    ArrayList<String> aufsichtsrat1 = new ArrayList<String>();
    aufsichtsrat1.add(STEFAN);
    aufsichtsrat1.add(RONALDO);

    JuristischePerson juristischePerson = new JuristischePerson(OSMAN, BRANCHE, aufsichtsrat1);
    EinzelUnternehmen einzelUnternehmen1 = new EinzelUnternehmen(STEFAN, BRANCHE2,
        true);
    Personengesselschaft personengesselschaft = new Personengesselschaft(MESSI, BRANCHE2,
        2,3);

    ArrayList<Unternehmen> unternehmenArrayList =  new ArrayList<Unternehmen>(
        Arrays.asList(juristischePerson, einzelUnternehmen1));

    FinanzAmt finanzAmt = new FinanzAmt(FINANZAMT_NAME, unternehmenArrayList, 0.0);

    finanzAmt.unternhmenHinzufuegen(personengesselschaft);





  }
}
