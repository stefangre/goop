import java.util.Objects;
import java.util.StringJoiner;

public class EinzelUnternehmen extends Unternehmen {

  private static final char EU_PREFIX = 'E';
  private boolean vorsteuerberechtigt;

  public EinzelUnternehmen(final String name, final Branche branche, final boolean vorsteuerberechtigt) {
    super(name, false, branche, EU_PREFIX);
    this.vorsteuerberechtigt = vorsteuerberechtigt;
  }


  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EinzelUnternehmen that = (EinzelUnternehmen) o;
    return vorsteuerberechtigt == that.vorsteuerberechtigt;
  }

  @Override
  public int hashCode() {
    return Objects.hash(vorsteuerberechtigt);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ",  "[", "]")
        .add(super.getKennung()+"")
        .add(super.getName()+"")
        .add(super.getBranche()+"")
        .add(super.getSteuersatz()+"")
        .add(super.getKontoAufwand()+"")
        .add(super.getKontoErtrag()+"")
        .add(vorsteuerberechtigt+"")
        .toString();
  }
}
