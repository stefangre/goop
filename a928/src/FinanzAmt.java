import java.util.ArrayList;
import java.util.Objects;

public class FinanzAmt {

  private final String name;
  private ArrayList<Unternehmen> kundenliste;
  private double steuerPot; //differenz zwischen vst und umst
  private double vstPot;
  private double umstPot;
  public FinanzAmt(final String name, final ArrayList<Unternehmen> kundenliste,
                   final double steuerPot) {
    this.name = name;
    this.kundenliste = kundenliste;
    this.steuerPot = steuerPot;
  }

  public ArrayList<Unternehmen> getKundenliste() {
    return kundenliste;
  }

  public void unternhmenHinzufuegen(final Unternehmen unternehmen) {
    String error = "";

    if (!Utility.objectNotNull(unternehmen)) {
      error += "Unternehmen darf nciht null sein";
    }
    if (kundenliste.contains(unternehmen)) {
      error += "Unternehmen ist schon in Liste";
    }
    if (Utility.stringNotNullOrEmpty(error)) {
      throw new IllegalArgumentException(error);
    }
    unternehmen.setFinanzAmt(this);
    unternehmen.setStatus(true);
    kundenliste.add(unternehmen);
  }

  public ArrayList<Unternehmen> zugeewieseneFirmen() {
    return new ArrayList<>(kundenliste);
  }


  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FinanzAmt finanzAmt = (FinanzAmt) o;
    return Double.compare(finanzAmt.steuerPot, steuerPot) == 0 &&
        Double.compare(finanzAmt.vstPot, vstPot) == 0 &&
        Double.compare(finanzAmt.umstPot, umstPot) == 0 &&
        name.equals(finanzAmt.name) &&
        kundenliste.equals(finanzAmt.kundenliste);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, kundenliste, steuerPot, vstPot, umstPot);
  }


}
