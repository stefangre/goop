import java.util.ArrayList;

public class JuristischePerson extends Unternehmen {

  private ArrayList<String> aufsichtsrat;
  private static final char JP_PREFIX = 'J';


  public JuristischePerson(final String name, final Branche branche, final ArrayList<String> aufsichtsrat) {
    super(name, false, branche, JP_PREFIX);
    this.aufsichtsrat = aufsichtsrat;
  }
}
