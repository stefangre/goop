public class Main {
  public static void main(final String[] args) {
    // Erzeugen einer Instanz der Businesslogik
    Controller ctrl = new Controller();
    // Starten der Businesslogik
    ctrl.create();
  }
}
