import java.util.ArrayList;

public class Personengesselschaft extends Unternehmen {

  private int anzahlKompl;
  private int anzahlKomandi;
  private static final char PG_PREFIX = 'P';

  public Personengesselschaft(final String name, final Branche branche, final int anzahlKompl, final int anzahlKomandi) {
    super(name, false, branche, PG_PREFIX);
    this.anzahlKompl = anzahlKompl;
    this.anzahlKomandi = anzahlKomandi;
  }
}
