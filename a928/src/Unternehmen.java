public class Unternehmen {

  private static int idCounter = 0;
  private String name;
  private boolean status;
  private String kennung;
  private char prefix;
  private Branche branche;
  private double steuersatz;

  private double DEFAULT_KOERPERsCHAFT = 0.25;

  private double DEFAULT_EINKOMMEN = 0.30;

  private double kontoErtrag;

  private FinanzAmt finanzAmt;
  private double kontoAufwand;

  public Unternehmen(final String name, final boolean status, final Branche branche, final char prefix) {

    String error = "";

    if (!Utility.stringNotNullOrEmpty(name)) {
      error += getNameExceptionMessage();
    }

    isValidPrefix(prefix);

    if (!Utility.charNotEmpty(prefix) && !isValidPrefix(prefix)) {
      error += getPrefixExceptionMessage();
    }
    if (!Utility.objectNotNull(branche)) {
      error += getBrancheExceptionMessage();
    }

    if (Utility.stringNotNullOrEmpty(error)) {
      throw new IllegalArgumentException(error);
    }

    this.name = name;
    this.status = status;
    this.branche = branche;

    this.kennung = this.prefix + String.valueOf(idCounter++);
  }

  public double getKontoErtrag() {
    return kontoErtrag;
  }

  public double getKontoAufwand() {
    return kontoAufwand;
  }

  private boolean isValidPrefix(final char prefix) {
    if (prefix == 'E' || prefix == 'P') {
      steuersatz = DEFAULT_EINKOMMEN;
    } else if (prefix == 'J') {
      steuersatz = DEFAULT_KOERPERsCHAFT;
    } else {
      return false;
    }
    return true;
  }

  private String getPrefixExceptionMessage() {
    return "prefix not valid.";
  }

  public void setFinanzAmt(final FinanzAmt finanzAmt) {
    this.finanzAmt = finanzAmt;
  }

  public double getSteuersatz() {
    return steuersatz;
  }

  private String getNameExceptionMessage() {
    return "name not valid.";
  }

  private String getBrancheExceptionMessage() {
    return "brnache is null.";
  }


  public String getName() {
    return name;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(final boolean status) {
    this.status = status;
  }

  public String getKennung() {
    return kennung;
  }

  public Branche getBranche() {
    return branche;
  }

  public boolean unternehmenEntfernen() {
    String error = "";

    if (!Utility.objectNotNull(this.finanzAmt.getKundenliste())) {
      error += "Kundeliste leer";
    } else {
      if (this.status) {
        if (this.finanzAmt.getKundenliste().contains(this)) {
          this.finanzAmt.getKundenliste().remove(this);
        } else {
          error += "Kundeliste beinhaltet Unternehmen nicht";
        }
      } else {
        error += "Unternehmen nciht gemeldet";
      }
    }

    if (Utility.stringNotNullOrEmpty(error)) {
      throw new IllegalArgumentException(error);
    }
    return true;
  }
}
