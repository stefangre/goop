package at.ac.fhvie.w19.goop3bb.k001.greil;

import java.util.ArrayList;
import java.util.Arrays;

class Controller {

  private static final String NAME_EMU = "Emu";
  private static final String NAME_STIESHUHN = "Taube";
  private static final String NAME_EULE = "Schleiereule";
  private static final String NAME_FISCH = "Fischadler";
  private static final String NAME_AV1 = "AV1";
  private static final String NAME_AV2 = "AV2";

  private static final int GROESSE_EMU = 180;
  private static final int GROESSE_STIESHUHN = 60;
  private static final int GROESSE_EULE = 34;
  private static final int GROESSE_FISCH = 66;

  private static final int FSW_EMU = 80;
  private static final int FSW_STIESHUHN = 70;
  private static final int FSW_EULE = 92;
  private static final int FSW_FISCH = 174;

  private static final int MAX_BE_AV1 = 5;
  private static final int MAX_BE_AV2 = 7;


  private static final int VISITOR_COUNT_RIESENRAD = 1000000;

  private static final String ILLEGAL_VALUE_DESCRIPTION = "";
  private static final int ILLEGAL_VALUE_GESTATION_IN_DAYS = -1;
  private static final double ILLEGAL_VALUE_AVARAGE_BIRTHS = -20.5;

  void create() {

    System.out.println("KONTINENT: ");
    Kontinent asien = Kontinent.ASIEN;
    System.out.println(asien);

    System.out.println("Laufvogel: ");
    LaufVogel emu = new LaufVogel(NAME_EMU, GROESSE_EMU, Kontinent.AUSTRALIEN, false);
    LaufVogel stieshuhn = new LaufVogel(NAME_STIESHUHN, GROESSE_STIESHUHN, Kontinent.AMERIKA,
        true);

    System.out.println(emu);
    System.out.println(stieshuhn);

    System.out.println("Flugvogel: ");
    FlugVogel eule = new FlugVogel(NAME_EULE, GROESSE_EULE, Kontinent.EUROPA, FSW_EULE);
    FlugVogel fisch = new FlugVogel(NAME_FISCH, GROESSE_FISCH, FSW_FISCH);
    FlugVogel fisch2 = new FlugVogel(NAME_FISCH, GROESSE_FISCH, Kontinent.AUSTRALIEN, FSW_FISCH);

    System.out.println(eule);
    System.out.println(fisch);

    ArrayList<Vogel> voegelAv1 = new ArrayList<Vogel>(
        Arrays.asList(emu, stieshuhn));

    ArrayList<Vogel> voegelAv2 = new ArrayList<Vogel>(
        Arrays.asList(eule, fisch));

    ArrayList<Vogel> voegelAv3 = new ArrayList<Vogel>(
        Arrays.asList(fisch2, null, fisch, null));

    System.out.println("Voiliere: ");
    Volliere av1 = new Volliere(voegelAv1, NAME_AV1, MAX_BE_AV1);
    Volliere av2 = new Volliere(voegelAv2, NAME_AV2, MAX_BE_AV2);

    System.out.println(av1);
    System.out.println(av2);

    av1.neuerVogel(fisch);
    System.out.println(av2);

    System.out.println(av2.neueVoegel(voegelAv1));
    System.out.println(av2);

    System.out.println(av1.neueVoegel(voegelAv3));
    System.out.println(av1);

    System.out.println("Nur flugvögel");
    System.out.println(av1.getFlugvoegel());

  }
}
