package at.ac.fhvie.w19.goop3bb.k001.greil;

import java.util.Objects;
import java.util.StringJoiner;

public class FlugVogel extends Vogel {

  private static final Kontinent DEFAULT_KONTINENT = Kontinent.EUROPA;
  private double fluegelSpannWeiteInCm;

  public FlugVogel(final String name, final double groeseInCm, final double fluegelSpannWeiteInCm) {
    this(name, groeseInCm, DEFAULT_KONTINENT, fluegelSpannWeiteInCm);
  }

  public FlugVogel(final String name, final double groeseInCm, final Kontinent kontinent,
                   final double fluegelSpannWeiteInCm) {
    super(name, groeseInCm, kontinent);

    String error = "";

    if (!Utility.doubleNotZeroOrNegativ(fluegelSpannWeiteInCm)) {
      error += getGluegelSpannWeiteInCmExceptionMessage();
    }
    if (Utility.stringNotNullOrEmpty(error)) {
      throw new IllegalArgumentException(error);
    }

    this.fluegelSpannWeiteInCm = fluegelSpannWeiteInCm;
  }

  private String getGluegelSpannWeiteInCmExceptionMessage() {
    return "fluegelSpannWeiteInCm darf nicht negativ oder null sein.";
  }


  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    FlugVogel flugVogel = (FlugVogel) o;
    return Double.compare(flugVogel.fluegelSpannWeiteInCm, fluegelSpannWeiteInCm) == 0;
  }

  @Override
  public int hashCode() {
    return Objects.hash(fluegelSpannWeiteInCm);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", FlugVogel.class.getSimpleName() + "[", "]")
        .add("ID=" + super.getId() + "/")
        .add("Name='" + super.getName() + "'/")
        .add("Kontinent=" + super.getKontinent() + "/")
        .add("Größe=" + super.getGroeseInCm() + "/")
        .add("Spannweite=" + fluegelSpannWeiteInCm)
        .toString();
  }
}


