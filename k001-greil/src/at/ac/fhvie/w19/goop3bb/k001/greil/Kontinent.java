package at.ac.fhvie.w19.goop3bb.k001.greil;

import java.util.StringJoiner;

public enum Kontinent {

  AMERIKA("Amerika"), AUSTRALIEN("Australien"), AFRIKA("Afrika"), ASIEN("Asien"), EUROPA("Europa");


  private String name;

  Kontinent(final String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ")
        .add(name)
        .toString();
  }
}

