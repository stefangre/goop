package at.ac.fhvie.w19.goop3bb.k001.greil;

import java.util.Objects;
import java.util.StringJoiner;

public class LaufVogel extends Vogel {

  private final boolean isFluegfaehig;

  public LaufVogel(final String name, final double groeseInCm, final Kontinent kontinent,
                   final boolean isFluegfaehig) {
    super(name, groeseInCm, kontinent);
    this.isFluegfaehig = isFluegfaehig;
  }


  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LaufVogel laufVogel = (LaufVogel) o;
    return isFluegfaehig == laufVogel.isFluegfaehig;
  }

  @Override
  public int hashCode() {
    return Objects.hash(isFluegfaehig);
  }

  @Override
  public String toString() {
    return new StringJoiner("")
        .add("ID=" + super.getId() + "/")
        .add("Name='" + super.getName() + "'/")
        .add("Kontinent=" + super.getKontinent() + "/")
        .add("Größe=" + super.getGroeseInCm() + "/")
        .add("Begrenzt flugfähig=" + isFluegfaehig)
        .toString();
  }
}


