package at.ac.fhvie.w19.goop3bb.k001.greil;

final class Main {

  private Main() {
  }

  public static void main(final String[] args) {
    Controller ctrl = new Controller();
    ctrl.create();
  }
}

/*
@startuml


abstract class Vogel{
- {static} EX_MESSAGE_CONSTRUCTION_YEAR : String =  "The construction year have to be higher ..."
- {static} MAGIC_COUNTER : int = 2000
- {static} idCounter : int = START_COUNTER
- String : name
- Kontinent : kontinent
- id : int
- groeseInCm : double
- constructionYear : int
--
+ Vogel(name: String, groeseInCm double, kontinent Kontinent)
--
+ getName() : int
+ getId() : int
+ getKontinent() : Kontinent
+ getNameExceptionMessage() : String
+ getGroeseInCmExceptionMessage() : String
+ getKontinentExceptionMessage() : String
+ equals(Object) : boolean
+ hashCode() : int
+ toString() : String
}

class Flugvogel extends Vogel {
- {static} Kontinent DEFAULT_KONTINENT
- double : fluegelSpannWeiteInCm
--

    + FlugVogel(name: String, groeseInCm double, fluegelSpannWeiteInCm double)
    + FlugVogel(name: String, groeseInCm double, Kontinent kontinent, fluegelSpannWeiteInCm double)

--
    + getGluegelSpannWeiteInCmExceptionMessage() : String

+ equals(Object) : boolean
+ hashCode() : int
+ toString() : String
}

class LaufVogel extends Vogel {
- {static} Kontinent DEFAULT_KONTINENT
- boolean : isFluegfaehig
--
    + LaufVogel(name: String, groeseInCm double, isFluegfaehig boolean)
--

+ equals(Object) : boolean
+ hashCode() : int
+ toString() : String
}

enum kontinent{
  AMERIKA(String name)
  AUSTRALIEN(String name)
  AFRIKA(String name)
  ASIEN(String name)
  EUROPA(String name)
- name : String
--
    + Kontinent(String name)
--

+ toString() : String
  }




class Volliere{
- String : name
- ArrayList<Vogel> : voegel
- id : int
- bezeichnung : String
- maxAnzahlVogel : int
--
+ Volliere(voegel: ArrayList<Vogel> , bezeichnung String, maxAnzahlVogel int)
--
+ listValid() : boolean
+ neuerVogel(vogel Vogel ) : String
+ neueVoegel(voegel ArrayList<Vogel> ) : boolean
+ getMaxAnzahlVogelExceptionMessage() : String
+ getFlugvoegel():ArrayList<FlugVogel>
+ getListExceptionMessage() : String
+ equals(Object) : boolean
+ hashCode() : int
+ toString() : String
}


class Zoo{

}
@enduml
 */