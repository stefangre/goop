package at.ac.fhvie.w19.goop3bb.k001.greil;

public final class Utility {

  private Utility() {

  }

  public static boolean stringNotNullOrEmpty(final String value) {
    return ((value != null) && (!"".equals(value)));
  }

  public static boolean objectNotNull(final Object object) {
    return (object != null);
  }

  public static boolean doubleNotZeroOrNegativ(final double value) {
    return !(value <= 0.0);
  }

  public static boolean doubleNotNegativ(final double value) {
    return !(value < 0.0);
  }

  public static boolean doubleNotZero(final double value) {
    return !(value == 0.0);
  }

  public static boolean intNotZeroOrNegativ(final int value) {
    return !(value <= 0);
  }

  public static boolean intNotNegativ(final int value) {
    return !(value < 0);
  }

  public static boolean intNotZero(final int value) {
    return !(value == 0);
  }

  public static boolean gtInt(final int valueOne, final int valueTwo) {
    return valueOne > valueTwo;
  }

  public static boolean gtEqualInt(final int valueOne, final int valueTwo) {
    return valueOne >= valueTwo;
  }

  public static boolean ltInt(final int valueOne, final int valueTwo) {
    return valueOne < valueTwo;
  }

  public static boolean ltEqualsInt(final int valueOne, final int valueTwo) {
    return valueOne <= valueTwo;
  }

  public static boolean gtDouble(final double valueOne, final double valueTwo) {
    return valueOne > valueTwo;
  }

  public static boolean gtEqualsDouble(final double valueOne, final double valueTwo) {
    return valueOne >= valueTwo;
  }

  public static boolean ltDouble(final double valueOne, final double valueTwo) {
    return valueOne < valueTwo;
  }

  public static boolean ltEqualsDouble(final double valueOne, final double valueTwo) {
    return valueOne <= valueTwo;
  }

}