package at.ac.fhvie.w19.goop3bb.k001.greil;

import java.util.Objects;
import java.util.StringJoiner;

public abstract class Vogel {

  private static final int MAGIC_COUNTER = 20000;
  private static int idCounter = MAGIC_COUNTER;
  private final String name;
  private final Kontinent kontinent;
  private final int id;
  private double groeseInCm;


  public Vogel(final String name, final double groeseInCm, final Kontinent kontinent) {

    String error = "";

    if (!Utility.stringNotNullOrEmpty(name)) {
      error += getNameExceptionMessage();
    }

    if (!Utility.doubleNotNegativ(groeseInCm)) {
      error += getGroeseInCmExceptionMessage();
    }

    if (!Utility.objectNotNull(kontinent)) {
      error += getKontinentExceptionMessage();
    }
    if (Utility.stringNotNullOrEmpty(error)) {
      throw new IllegalArgumentException(error);
    }


    this.name = name;
    this.groeseInCm = groeseInCm;
    this.kontinent = kontinent;

    this.id = idCounter++;

  }


  public String getName() {
    return name;
  }

  public Kontinent getKontinent() {
    return kontinent;
  }

  public int getId() {
    return id;
  }

  public double getGroeseInCm() {
    return groeseInCm;
  }

  private String getNameExceptionMessage() {
    return "name nicht gefunden.";
  }

  private String getGroeseInCmExceptionMessage() {
    return "groeseInCm ist nicht größer gleich null.";
  }

  private String getKontinentExceptionMessage() {
    return "Kontinent nicht gefunden.";
  }


  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Vogel vogel = (Vogel) o;
    return id == vogel.id &&
        Double.compare(vogel.groeseInCm, groeseInCm) == 0 &&
        Objects.equals(name, vogel.name) &&
        kontinent == vogel.kontinent;
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, kontinent, id, groeseInCm);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Vogel.class.getSimpleName() + "[", "]")
        .add("name='" + name + "'")
        .add("kontinent=" + kontinent)
        .add("id=" + id)
        .add("groeseInCm=" + groeseInCm)
        .toString();
  }
}








