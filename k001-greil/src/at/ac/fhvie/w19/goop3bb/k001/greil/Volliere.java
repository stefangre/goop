package at.ac.fhvie.w19.goop3bb.k001.greil;

import java.util.ArrayList;
import java.util.Objects;
import java.util.StringJoiner;

public class Volliere {

  private static int idCounter = 1;
  private final ArrayList<Vogel> voegel;
  private final int id;
  private final String bezeichnung;
  private final int maxAnzahlVogel;


  public Volliere(final ArrayList<Vogel> voegel, final String bezeichnung,
                  final int maxAnzahlVogel) {

    String error = "";

    if (!Utility.stringNotNullOrEmpty(bezeichnung)) {
      error += getBezeichnungExceptionMessage();
    }

    if (!Utility.intNotZeroOrNegativ(maxAnzahlVogel)) {
      error += getMaxAnzahlVogelExceptionMessage();
    }
    if (!listValid(voegel)) {
      error += getListExceptionMessage();
    }

    if (Utility.stringNotNullOrEmpty(error)) {
      throw new IllegalArgumentException(error);
    }

    this.voegel = new ArrayList<>(voegel);

    this.bezeichnung = bezeichnung;
    this.maxAnzahlVogel = maxAnzahlVogel;

    this.id = idCounter++;
  }


  private String getBezeichnungExceptionMessage() {
    return "Bezeichnung nicht gefunden.";
  }

  private String getMaxAnzahlVogelExceptionMessage() {
    return "Max Anazahl an Voegel keine positive Zahl.";
  }

  private String getListExceptionMessage() {
    return "List nicht valide.";
  }

  private boolean listValid(final ArrayList<Vogel> voegel) {
    if (voegel != null && voegel.isEmpty()) {
      return false;
    }
    return true;
  }

  public void neuerVogel(final Vogel neuerVogel) {
    if (listValid(this.voegel) && Utility.objectNotNull(neuerVogel)) {
      this.voegel.add(neuerVogel);
    } else {
      throw new IllegalArgumentException("Vogel konnte nicht hinzugefügt werden");
    }
  }

  public boolean neueVoegel(final ArrayList<Vogel> voegel) {
    String error = "";

    if (!Utility.objectNotNull(voegel)) {
      return false;
    }

    for (Vogel vogel : voegel) {
      if (!Utility.objectNotNull(voegel)) {
        return false;
      }
      if (this.voegel.contains(vogel)) {
        return false;
      }
    }

    this.voegel.addAll(voegel);
    return true;
  }


  public ArrayList<Vogel> getFlugvoegel() {
    ArrayList<Vogel> returnVoegel = new ArrayList<>();

    if (!listValid(this.voegel)) {
      throw new IllegalArgumentException("Vogel konnte nicht hinzugefügt werden");
    }

    for (Vogel vogel : this.voegel) {
      if (vogel instanceof FlugVogel) {
        returnVoegel.add(vogel);
      }
    }
    return returnVoegel;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Volliere volliere = (Volliere) o;
    return id == volliere.id
        && maxAnzahlVogel == volliere.maxAnzahlVogel
        && Objects.equals(voegel, volliere.voegel)
        && Objects.equals(bezeichnung, volliere.bezeichnung);
  }

  @Override
  public int hashCode() {
    return Objects.hash(voegel, id, bezeichnung, maxAnzahlVogel);
  }

  private String printVoegelList(final ArrayList<Vogel> voegelList) {
    String returnvalue = "";
    for (Vogel vogel : voegelList) {
      returnvalue += vogel.toString() + ",";
    }
    return returnvalue;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Volliere.class.getSimpleName() + "[", "]")
        .add("ID: " + id)
        .add(" Name: '" + bezeichnung + "'")
        .add(" maximale Belegung " + maxAnzahlVogel)
        .add(printVoegelList(voegel))
        .toString();
  }
}


