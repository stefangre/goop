package at.ac.fhvie.w19.goop3bb.m001.murth;


public class Controller {

  public void start() {
    System.out.println();
    System.out.println("\n +++ Die Formate und deren Fläche(n):");
    for (Size size : Size.values()) {
      System.out.println("  # " + size + " : " + size.area() + " m2");
    }

    System.out.println();
    System.out.println("\n +++ Die Kartons und ihre Preise:");
    for (Size size : Size.values()) {
      for (Thickness thickness : Thickness.values()) {
        OrderPosition position = new OrderPosition(1, size, thickness);
        System.out.printf("  * %-22s : %4.2f €\n", position, position.cost());
      }
    }

    System.out.println();

    // Kunde erzeugt und richtig ausgegeben
    // ACHTUNG: HIER WIRD KEIN KONSTRUKTOR AUFGERUFEN,
    // SONDERN EINE METHODE
    Customer primeCustomer = newCustomer("Zappadoing");

    // Kein doppelter Kundenname - liefert null, braucht nicht gespeichert werden
    newCustomer("Zappadoing");

    newOrder(primeCustomer);            // Bestellung bei Kunden
    newOrder(primeCustomer);            // Keine zweite aktuelle Bestlelung

    addOrderPosition(primeCustomer, 33, Size.MAXI, Thickness.DUENN);        // Noch kein Guthaben
    addToCredit(primeCustomer, -120);        // falscher Betrag
    addToCredit(primeCustomer, 120);            // passt
    addOrderPosition(primeCustomer, 33, Size.MAXI, Thickness.DUENN);        // Jetzt ist Guthaben da

    addOrderPosition(primeCustomer, 100, Size.MIDI, Thickness.DICK);        // zweiter Posten
    addOrderPosition(primeCustomer, 50, Size.MIDI, Thickness.DICK);        // Posten zusammenlegen

    Customer goodCustomer = newCustomer("Huberbauer");        // Noch ein Kunde
    finalizeOrder(primeCustomer);
    finalizeOrder(primeCustomer);        // kein doppelter Abschlusss
    finalizeOrder(goodCustomer);        // hat noch keine aktuelle Bestellung

    // Die alte Bestellung vom ersten Kunden sollte jetzt aufscheinen
    Customer.salesStatistic();

    newOrder(primeCustomer);        // geht das auch ein zweites Mal
    addOrderPosition(primeCustomer, 10, Size.MAXI, Thickness.DICK);        // Zu teuer
    addOrderPosition(primeCustomer, 5, Size.MAXI, Thickness.DICK);        // Geht sich aus

    SpecialCustomer specialCustomer = newSpecialCustomer("Wippredall");        // ebenso SonderKunde
    newOrder(specialCustomer);    // Der hat kein Guthaben, aber einen Rahmen
    addOrderPosition(specialCustomer, 10, Size.MAXI, Thickness.DICK);
    addOrderPosition(specialCustomer, 10, Size.MIDI, Thickness.DICK);
    addOrderPosition(specialCustomer, 10, Size.MAXI, Thickness.DICK);

    addToCredit(goodCustomer, 100);
    addOrderPosition(goodCustomer, 20, Size.MIDI, Thickness.MITTEL);    // Posten ohne Bestellung

    // geht die Statistik auch mit mehreren KUnden und Bestellungen
    Customer.salesStatistic();

    finalizeOrder(primeCustomer);
    finalizeOrder(specialCustomer);

    Customer.salesStatistic();


  }

  // Neuen Kunden anlegen
  private Customer newCustomer(final String name) {
    Customer customer = null;
    System.out.println("\n");
    try {
      customer = new Customer(name);
      System.out.println("*** Neuer Kunde angelegt: " + customer);
    } catch (Exception exc) {
      printException(exc);
    }
    return customer;
  }

  private SpecialCustomer newSpecialCustomer(final String name) {
    SpecialCustomer specialCustomer = null;
    System.out.println("\n");
    try {
      specialCustomer = new SpecialCustomer(name);
      System.out.println(" *** Neuer SonderKunde angelegt: " + specialCustomer);
    } catch (Exception exc) {
      printException(exc);
    }
    return specialCustomer;
  }

  private void newOrder(final Customer customer) {
    System.out.println("\n");
    try {
      customer.newOrder();
      System.out.println(" *** Bestellung hinzugefügt: " + customer);
    } catch (Exception exc) {
      printException(exc);
    }
  }

  public void addOrderPosition(final Customer customer, final int count,
                               final Size size, final Thickness thickness) {
    System.out.println("\n");
    try {
      customer.addOrderPosition(count, size, thickness);
      System.out.println(" *** Posten hinzugefügt: " + customer);
    } catch (Exception exc) {
      printException(exc);
    }
  }

  public void finalizeOrder(final Customer customer) {
    System.out.println("\n");

    try {
      System.out.print(" *** Bestellung abschliessen: ");
      customer.finalizeOrder();
    } catch (Exception exc) {
      printException(exc);
    }
  }

  public void addToCredit(final Customer customer, final float amount) {
    System.out.println("\n");

    try {
      customer.increaseCreditBy(amount);
      System.out.println(" *** Neues Guthaben von " + customer.getName()
          + ": " + customer.getCredit());
    } catch (Exception exc) {
      printException(exc);
    }
  }

  private void printException(final Exception exc) {
    System.out.println(" !!! " + exc.getLocalizedMessage());
  }


}











