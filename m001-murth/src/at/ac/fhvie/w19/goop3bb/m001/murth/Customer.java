package at.ac.fhvie.w19.goop3bb.m001.murth;

import java.util.ArrayList;

public class Customer {
  /**
   * Liste aller erstellten Kunden.
   * Wichtig ist hier, dass diese Liste static ist!!!!!!
   */
  private static ArrayList<Customer> customers = new ArrayList<>();

  /**
   * Kundenname
   */
  private final String name;

  /**
   * Finanzrahmen
   */
  private float credit;

  /*
  Aktuelle Bestellung
   */
  private Order currentOrder;

  /*
  Liste aller vergangenen Bestellungen
   */
  private ArrayList<Order> oldOrders = new ArrayList<>();

  // Konstruktor
  public Customer(final String name) {
    String error = "";

    // Test ob der Name leer oder NULL ist
    if (Utility.isEmptyString(name)) {
      error += nameExceptionMessage();
    }

    // Wenn ein Fehler vorliegt, wird eine Exception geworfen
    if (!error.isEmpty()) {
      throw new IllegalArgumentException(error);
    }

    // Test ob es in der Liste der Kunden bereits einen Kunden mit dem gleichen
    // Namen gibt.
    for (Customer customer : customers) {
      if (customer.name.equals(name)) {
        throw new IllegalArgumentException("Der Name " + name + " ist bereits vorhanden.");
      }
    }

    this.name = name;

    // Jeder Kunde hat am Anfang ein Guthaben von 0
    this.setCredit(0);

    // Aktuellen Kunden zur Liste der Kunden hinzufügen
    customers.add(this);
  }

  private String nameExceptionMessage() {
    return "Name darf nicht leer sein";
  }

  // Setzen des neuen Guthabens, welches nicht negativ sein darf
  public void setCredit(final float credit) {
    if (!isValidCredit(credit)) {
      throw new IllegalArgumentException("Das Guthaben darf nicht negativ sein.");
    }

    this.credit = credit;
  }

  public String getName() {
    return name;
  }

  public float getCredit() {
    return credit;
  }

  // Laut Angabe addToGuthaben() Beispiel 3.1
  // Aus der Angabe geht nicht klar hervor, dass das neue Guthaben zurückgegeben werden soll.
  public float increaseCreditBy(final float amount) {

    // Der Betrag um den erhöht wird darf nicht negativ sein.
    if (Utility.isGreaterZero(amount)) {
      throw new IllegalArgumentException("Der Betrag darf nicht negativ sein");
    }

    credit += amount;
    // Es wird das neue Guthaben zurückgegeben, was nicht ganz aus der Angabe herausgeht
    return credit;
  }

  // Test ob der Guthabenbetrag valide ist
  public boolean isValidCredit(final float value) {
    return value >= 0;
  }

  // Laut Angabe neueBestellung() Beispiel 3.2
  public void newOrder() {
    if (currentOrder != null) {
      throw new IllegalArgumentException("Der Kunde " + name
          + " hat schon eine aktuelle Bestellung.");
    }

    currentOrder = new Order(this);
  }

  // Laut Angabe addPosten() Beispiel 3.3
  // Welcher Konstruktor gefordert ist, geht nicht aus der Angabe hervor
  public float addOrderPosition(final int count, final Size size,
                                final Thickness thickness) {
    return this.addOrderPosition(new OrderPosition(count, size, thickness));
  }

  // Laut Angabe addPosten() Beispiel 3.3
  // Welcher Konstruktor gefordert ist, geht nicht aus der Angabe hervor
  public float addOrderPosition(final OrderPosition orderPosition) {
    float cost;

    // Neue Position darf nur hinzugefügt werden, wenn es einen aktuellen Auftrag
    // gibt.
    if (currentOrder == null) {
      throw new IllegalArgumentException("Derzeit keine aktuelle Bestellung vorhanden.");
    }

    // Ermitteln der Kosten aus dem bestehenden Auftrag plus dem Wert der
    // hinzuzufügenden Position
    cost = currentOrder.cost() + orderPosition.cost();

    // Prüfung ob der Auftrag plus die neue Auftragsposition nicht das
    // Guthaben übersteigt.
    if (!isAcceptedCost(cost)) {
      throw new IllegalArgumentException("Nicht mehr genug Guthaben vorhanden.");
    }

    currentOrder.addOrderPosition(orderPosition);

    return cost;
  }

  // Laut Angabe abschliessen() 3.5
  public void finalizeOrder() {

    // Es muss eine aktive Bestellung geben
    if (currentOrder == null) {
      throw new IllegalArgumentException("Derzeit keine aktuelle Bestellung vorhanden.");
    }

    // Auftrag sperren
    currentOrder.sperren();

    // Abziehen der Bestellsumme vom Guthaben
    setCredit(getCredit() - currentOrder.cost());

    // Abgeschlossenen Auftrag zur Liste aller Aufträge hinzufügen
    oldOrders.add(currentOrder);

    // Zurücksetzen des aktuellen Auftrages
    currentOrder = null;

    // Ausgabe des Auftrags
    System.out.println(currentOrder);
  }

  // Test ob Kosten eines Auftrags nicht die Rahmensumme übersteigen
  public boolean isAcceptedCost(final float cost) {
    return cost <= credit;
  }

  // LAut Angabe verkaufteMenge() 3.7
  public int soldAmount(final Size groesse, final Thickness staerke) {
    int count = 0;

    // Iteration über alle alten Aufträge
    for (Order order : oldOrders) {
      count += order.itemCount(groesse, staerke);
    }
    return count;
  }

  // Laut Angabe verkaufsStatistik() 3.8 ??
  // Ermittelt die Verkaufsstatistik ALLER Kunden
  // Wichtig ist hier, dass die Methode static ist und auch
  // die ArrayList aller angelegten Kunden static ist.
  public static void salesStatistic() {
    int count;

    System.out.println("\n******************************");
    System.out.println("***** Verkaufsstatistik: *****");
    // Iteration über alle möglichen Größen
    for (Size size : Size.values()) {
      // Iteration über alle möglichen Dicken
      for (Thickness thickness : Thickness.values()) {
        count = 0;

        // Iteration über alle Kunden.
        for (Customer customer : customers) {
          // Hier wird die Methode aus Aufgabe 3.7 aufgerufen
          count += customer.soldAmount(size, thickness);
        }
        // Ausgabe der Summe
        System.out.println(" * " + size + " " + thickness + " " + count + " Mal");
      }
    }
    System.out.println("******************************\n");
  }

  @Override
  public String toString() {
    float cost = 0;
    for (Order order : oldOrders) {
      cost += order.cost();
    }

    return "[" + name
        + " - " + oldOrders.size() + " Bestellungen "
        + " - " + cost + " € "
        + (currentOrder == null ? "keine aktuelle Bestellung" :
        "aktuelle Bestellung: " + currentOrder)
        + "]";
  }
}
