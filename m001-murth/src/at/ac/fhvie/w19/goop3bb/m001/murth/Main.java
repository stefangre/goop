package at.ac.fhvie.w19.goop3bb.m001.murth;

public final class Main {

  private Main() {

  }

  public static void main(final String[] args) {

    Controller controller = new Controller();
    controller.start();

  }
}

