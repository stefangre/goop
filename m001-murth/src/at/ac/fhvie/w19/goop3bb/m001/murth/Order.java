package at.ac.fhvie.w19.goop3bb.m001.murth;

import java.util.ArrayList;

public class Order {

  /**
   * Static Zähler für die Anzahl der erstellten Instanzen
   */
  private static int idCounter = 1;

  /**
   * Id der Instanz
   */
  private final int id;

  /**
   * Zum Auftrag gehörender Kunde
   */
  private final Customer kunde;

  /**
   * TRUE, wenn der Auftrag gesperrt ist
   */
  private boolean gesperrt;

  /**
   * Liste aller Positionen des Auftrags
   */
  private ArrayList<OrderPosition> orderPositions = new ArrayList<>();

  public Order(final Customer kunde) {

    // Test ob Kunde NULL ist
    if (kunde == null) {
      throw new IllegalArgumentException("Es muss ein Kunde angegeben werden.");
    }

    // Zuweisen der ID und inkrementieren des Zählers
    this.id = idCounter++;
    this.kunde = kunde;
    this.gesperrt = false;
  }

  // Sperren eines Kunden
  public void sperren() {
    this.gesperrt = false;
  }

  // Laut Angabe kosten() 3.4
  public float cost() {
    float cost = 0;

    // Iterieren über alle Positionen des Auftrages
    for (OrderPosition orderPosition : orderPositions) {
      // Aufsummieren der Positionskosten
      cost += orderPosition.cost();
    }

    return cost;
  }

  // Laut Angabe addPosten() 3.3
  public void addOrderPosition(final OrderPosition newOrderPosition) {
    boolean found = false;

    // Test ob Orderposition NULL ist
    if (newOrderPosition == null) {
      throw new IllegalArgumentException("Kein Posten angegeben.");
    }

    // Eine neue Position darf nur dann hinzugefügt werden, wenn die Bestellung
    // nicht gesperrt ist
    if (this.gesperrt) {
      throw new IllegalArgumentException("Die Bestellung ist gesperrt.");
    }

    // Es muss gesucht werden, ob es bereits eine Position mit der gleichen
    // Größe und Stärke gibt. Wenn ja, wird die Anzahl der neuen Auftragsposition
    // zur bestehenden hinzuaddiert.
    for (OrderPosition position : this.orderPositions) {
      // Test auf gleiche Größe und gleiche Stärke
      if (newOrderPosition.getSize().equals(position.getSize())
          && newOrderPosition.getThickness().equals(position.getThickness())) {
        // Es wurde eine Position mit den gleichen Werten entdeckt, daher
        // wird die Anzahl erhöht.
        position.setCount(position.getCount() + newOrderPosition.getCount());

        // Diese Variable wird TRUE gesetzt, damit später überprüft weren kann,
        // dass ein Eintrag gefunden wurde
        found = true;

        // Die Schleife wird unterbrochen, da es nur Zeitverschwendung ist
        // weiter zu machen
        break;
      }
    }

    // Nur wenn keine Position mit der gleichen Größe und Stärke gefunden wurde,
    // darf eine neue Position zur Bestellung hinzugefügt werden.
    if (!found) {
      this.orderPositions.add(newOrderPosition);
    }
  }

  // Laut Angabe wieOft() 3.6
  public int itemCount(final Size size, final Thickness thickness) {
    int count = 0;

    // Iteration über alle Positionen im Auftrag
    for (OrderPosition position : this.orderPositions) {
      // Überprüfung ob Größe und Dicke der Auftragsposition mit der
      // gewünschten Größe und Dicke übereinstimmen
      if (position.getSize().equals(size)
          && position.getThickness().equals(thickness)) {
        // Erhöhen der Summe um die Anzahl der in der Bestellposition bestellten
        // Stück
        count += position.getCount();
      }
    }

    return count;
  }

  @Override
  public String toString() {
    return "[Best-Nr. " + this.id + " (" + this.kunde.getName() + ") "
        + this.orderPositions.size() + " Posten " + this.cost()
        + " € " + this.orderPositions + "]";
  }
}
