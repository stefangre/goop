package at.ac.fhvie.w19.goop3bb.m001.murth;

public class OrderPosition {
  /**
   * Größe
   */
  private final Size size;

  /**
   * Stärke
   */
  private final Thickness thickness;

  /**
   * Anzahl
   */
  private int count;

  public OrderPosition(final int count, final Size size, final Thickness thickness) {
    String error = "";

    // Größe darf nicht NULL sein
    if (size == null) {
      error += "Größe darf nicht NULL sein";
    }

    // Stärke darf nicht NULL sein
    if (thickness == null) {
      error += "Stärke darf nicht NULL sein";
    }

    // Anzahl muss größer als 0 sein
    if (!isValidCount(count)) {
      error += countExceptionMessage(count);
    }

    // Eine Exception wird geworfen, wenn der Fehlerstring nicht leer ist.
    if (!error.isEmpty()) {
      throw new IllegalArgumentException(error);
    }

    this.size = size;
    this.thickness = thickness;
    this.count = count;
  }

  // Test ob der übergebene Wert für die Anzahl korrekt ist
  private boolean isValidCount(final int value) {
    return Utility.isGreaterZero(value);
  }

  // Exception Message, wenn der Wert für die Anzahl nicht korrekt ist.
  private String countExceptionMessage(final int errorValue) {
    return "Anzahl muss größer als 0 sein (Ist:" + errorValue + ")";
  }

  // Setter für die Anzahl der Position
  public void setCount(final int count) {
    if (!isValidCount(count)) {
      throw new IllegalArgumentException(countExceptionMessage(count));
    }

    this.count = count;
  }

  public Size getSize() {
    return this.size;
  }

  public Thickness getThickness() {
    return this.thickness;
  }

  public int getCount() {
    return this.count;
  }

  public float cost() {
    return this.count * this.size.area() * this.thickness.getPricePerUnit();
  }

  @Override
  public String toString() {
    return "[" + this.count + " Stück " + this.size + " " + this.thickness + "]";
  }
}
