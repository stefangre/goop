package at.ac.fhvie.w19.goop3bb.m001.murth;

public enum Size {
  MINI(10, 20),
  MIDI(20, 30),
  MAXI(30, 40);

  public static final int AREA_CONVERSION = 10000;
  private final float width;
  private final float length;

  Size(final float width, final float length) {
    this.width = width;
    this.length = length;
  }

  public float area() {
    return this.length * this.width / AREA_CONVERSION;
  }
}

