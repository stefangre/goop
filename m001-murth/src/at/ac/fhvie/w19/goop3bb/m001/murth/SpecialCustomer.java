package at.ac.fhvie.w19.goop3bb.m001.murth;

public class SpecialCustomer extends Customer {

  /**
   * Standard Überziehungsrahmen
   */
  private static final double STD_RAHMEN = 1000;

  /**
   * Überziehungsrahmen
   */
  private double overdraftFrame;

  public SpecialCustomer(final String name) {
    super(name);
    this.overdraftFrame = STD_RAHMEN;
  }

  public void setOverdraftFrame(final float overdraftFrame) {
    if (!(overdraftFrame >= 0)) {
      throw new IllegalArgumentException("Der Rahmen muss positiv sein.");
    }
    this.overdraftFrame = overdraftFrame;
  }

  public boolean isValidCredit(final float credit) {
    return credit >= this.overdraftFrame * (-1);
  }

  public double getOverdraftFrame() {
    return this.overdraftFrame;
  }

  // Die Methode muss überschrieben werden, da hier auch der Überziehungsrahmen
  // mit beachtet werden muss
  @Override
  public boolean isAcceptedCost(final float cost) {
    return cost <= getCredit() + overdraftFrame;
  }

  @Override
  public String toString() {
    return "[*SK* " + super.toString().substring(1);
  }
}
