package at.ac.fhvie.w19.goop3bb.m001.murth;

public enum Thickness {
  DUENN(5) {
    @Override
    public String toString() {
      return "dünn";
    }
  },
  MITTEL(7),
  DICK(10);

  private float pricePerUnit;

  Thickness(final float pricePerUnit) {
    this.pricePerUnit = pricePerUnit;
  }

  public float getPricePerUnit() {
    return this.pricePerUnit;
  }

  @Override
  public String toString() {
    return this.name().toLowerCase();
  }
}
