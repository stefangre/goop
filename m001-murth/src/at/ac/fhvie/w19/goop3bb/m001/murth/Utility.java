package at.ac.fhvie.w19.goop3bb.m001.murth;

public final class Utility {

  private Utility() {
  }

  public static boolean isEmptyString(final String value) {
    return (value == null || "".equals(value.trim()));
  }

  public static boolean isGreaterZero(final int value) {
    return (value > 0);
  }

  public static boolean isGreaterZero(final float value) {
    return (value > 0F);
  }

}
