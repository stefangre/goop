import java.util.StringJoiner;

public class Mammal {

  private static final double DEFAULT_AVARAGE_BIRTHS = 1.2;
  private static final int DEFAULT_GESTATION_IN_DAYS = 270;
  private static final String EX_MESSAGE_DESCRIPTION = "XXXXX must not be empty. \n";
  private static final String EX_MESSAGE_GESTATION_IN_DAYS = "XXXXX have to be higher than 0, actual value: ";
  private static final String EX_MESSAGE_AVARAGE_BIRTHS = "XXXXX have to be higher than 0, actual value: ";

  private String description;
  private int gestationInDays;
  private double averageBirths;

  Mammal(final String description) {
    this(description, DEFAULT_GESTATION_IN_DAYS, DEFAULT_AVARAGE_BIRTHS);
  }

  Mammal(final String description, final int gestationInDays) {
    this(description, gestationInDays, DEFAULT_AVARAGE_BIRTHS);
  }

  Mammal(final String description, final int gestationInDays, final double averageBirths) {
    boolean error = !(isValidString(description) && isValidPositveIntNumber(gestationInDays) && isValidPositveDoubleNumber(averageBirths));

    if (error) {
      String errorMessage = "";
      if (!isValidString(description)) {
        errorMessage = getDescriptionExceptionMessage();
      }
      if (!isValidPositveIntNumber(gestationInDays)) {
        errorMessage += getGestationInDaysExceptionMessage(gestationInDays);
      }
      if (!isValidPositveDoubleNumber(averageBirths)) {
        errorMessage += getAverageBirthsExceptionMessage(averageBirths);
      }
      throw new IllegalArgumentException(errorMessage);
    }

    this.description = description;
    this.gestationInDays = gestationInDays;
    this.averageBirths = averageBirths;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    if (!isValidString(description)) {
      throw new IllegalArgumentException(getDescriptionExceptionMessage());
    }
    this.description = description;
  }

  public int getGestationInDays() {
    return gestationInDays;
  }

  public void setGestationInDays(final int gestationInDays) {
    if (!isValidPositveIntNumber(gestationInDays)) {
      throw new IllegalArgumentException(getGestationInDaysExceptionMessage(gestationInDays));
    }
    this.gestationInDays = gestationInDays;
  }

  public double getAverageBirths() {
    return averageBirths;
  }

  public void setAverageBirths(final double averageBirths) {
    if (!isValidPositveDoubleNumber(averageBirths)) {
      throw new IllegalArgumentException(getAverageBirthsExceptionMessage(averageBirths));
    }
    this.averageBirths = averageBirths;
  }

  private boolean isValidString(final String value) {
    return !(value == null) && (!value.trim().isEmpty());
  }

  private boolean isValidPositveIntNumber(final int value) {
    return value >= 0;
  }

  private boolean isValidPositveDoubleNumber(final double value) {
    return value >= 0;
  }

  private String getDescriptionExceptionMessage() {
    return EX_MESSAGE_DESCRIPTION;
  }

  private String getGestationInDaysExceptionMessage(final int value) {
    return EX_MESSAGE_GESTATION_IN_DAYS + value + "\n";
  }

  private String getAverageBirthsExceptionMessage(final double value) {
    return EX_MESSAGE_AVARAGE_BIRTHS + value + "\n";
  }

  @Override
  public String toString() {
    //• {[type=love], [smell=turpentine], [look=india ink]}
    return new StringJoiner(", ", "• {", "}")
        .add("[description=" + description + "]")
        .add("[gestationInDays=" + gestationInDays + "]")
        .add("[averageBirths=" + averageBirths + "]")
        .toString();
  }
}

/*
@startuml
class Mammal{
- {static} DEFAULT_AVARAGE_BIRTHS : int = 1
- {static} DEFAULT_GESTATION_IN_DAYS : int = 270
- {static} EX_MESSAGE_DESCRIPTION : String = "description must not be empty."
- {static} EX_MESSAGE_GESTATION_IN_DAYS : String = "gestationInDays have to be higher than 0, actual value: "
- {static} EX_MESSAGE_AVARAGE_BIRTHS : String = "averageBirths have to be higher than 0, actual value: "
- description: String {notNull, notEmpty}
- gestationInDays: int {>0}
- averageBirths : double {>0}
--
+ Mammal(description : String)
+ Mammal(description : String, gestationInDays : int)
+ Mammal(description : String, gestationInDays : int, averageBirths : int)
--
+ getDescription() : String
+ setDescription(description: String)
+ setGestationInDays() : int
+ getGestationInDays(gestationInDays: int)
+ setAverageBirths() : double
+ getAverageBirths(averageBirths : double)
+ toString() : String
--
- isValidDescription(value : String) : boolean
- getDescriptionExceptionMessage() : String
- isValidGestationInDays(value : int) : boolean
- getGestationInDaysExceptionMessage(value : int) : String
- isValidAverageBirths(value : double) : boolean
- getAverageBirthsExceptionMessage(value : double) : String
}
@enduml
 */