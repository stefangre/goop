package at.ac.fhvie.w19.oopr2bb.a110.greil;

import java.util.Comparator;

public class AttractionGlobalIdComparator implements Comparator<Attraction> {

  @Override
  public int compare(final Attraction left, final Attraction right) {
    return left.compareTo(right);
  }
}

/*
@startuml
class AttractionGlobalIdComparator implements Comparator {
--
+ compare(left: Attraction, right Attraction) : int
}
@enduml
 */


