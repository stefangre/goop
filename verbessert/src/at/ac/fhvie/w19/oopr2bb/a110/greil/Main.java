package at.ac.fhvie.w19.oopr2bb.a110.greil;

// Lektor: Sie haben UML Diagramme von Klassen sowohl in der Attraction Klasse, als auch in den
// einzelnen Kindklassen. Das ist ein Problem.
// Machen Sie die Diagramme in den einzelnen Klassen und führen Sie dann das UML Diagramm in der
// Klasse Main oder Controller zusammen. In der Attraction-Klasse dies zu machen ist etwas unübersichtlich

final class Main {

  private Main() {
  }

  public static void main(final String[] args) {
    Controller ctrl = new Controller();
    ctrl.create();
  }
}
