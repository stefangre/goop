package at.ac.fhvie.w19.oopr2bb.a110.greil;

import java.util.StringJoiner;

public class Museum extends Attraction {

  private static final Alignment DEFAULT_ALIGMENT = Alignment.NATURWISSENSCHAFT;
  private static int idCounter = 1;
  private final int id;
  private Alignment alignment;

  public Museum(final String name, final int lastYearVisitorCount) {
    this(name, lastYearVisitorCount, DEFAULT_ALIGMENT);
  }

  public Museum(final String name, final int lastYearVisitorCount, final Alignment alignment) {
    super(name, lastYearVisitorCount);
    this.alignment = alignment;
    this.id = idCounter++;
  }

  public Alignment getAlignment() {
    return alignment;
  }

  public void setAlignment(final Alignment alignment) {
    this.alignment = alignment;
  }

  public int getId() {
    return id;
  }

  @Override
  public String toString() {
    //{type = “love“, smell = “turpentine“, look = “india ink“}
    return new StringJoiner(", ", "{", "}")
        .add("globalId = \"" + getGlobalId() + "\"")
        .add("id = \"" + id + "\"")
        .add("alignment = \"" + alignment + "\"")
        .toString();
  }

  // Lektor: equals() und hashCode()?
}

/*
@startuml
class Museum extends Attraction {
- {static} DEFAULT_ALIGMENT : Alignment = Alignment.NATURWISSENSCHAFT
- {static} idCounter : int = 1
- id : int
- alignment : Alignment
--
+ Museum(name: String, lastYearVisitorCount int)
+ Museum(name: String, lastYearVisitorCount: int, alignment : Alignment)
--
+ getAlignment() : Alignment
+ setAlignment(alignment : Alignment)
+ getId() : int
+ toString() : String
}
@enduml
 */