package at.ac.fhvie.w19.oopr2bb.a110.greil;

import java.util.StringJoiner;

public class Zoo extends Attraction {

  private static final String EX_MESSAGE_ANIMAL_TYPE_COUNTER =
      "The animal-type-counter have to be higher than 0";
  private static final int DEFAULT_ANIMAL_TYPE_COUNTER = 100;
  private static final int START_COUNTER = 3200;
  private static int idCounter = START_COUNTER;
  private final int id;
  private int animalTypeCounter;

  public Zoo(final String name, final int lastYearVisitorCount) {
    this(name, lastYearVisitorCount, DEFAULT_ANIMAL_TYPE_COUNTER);
  }

  public Zoo(final String name, final int lastYearVisitorCount, final int animalTypeCounter) {
    super(name, lastYearVisitorCount);
    if (!super.isValidPosNumber(animalTypeCounter)) {
      throw new IllegalArgumentException(EX_MESSAGE_ANIMAL_TYPE_COUNTER);
    }
    this.animalTypeCounter = animalTypeCounter;
    this.id = idCounter++;
  }

  public int getAnimalTypeCounter() {
    return animalTypeCounter;
  }

  public void setAnimalTypeCounter(final int animalTypeCounter) {
    this.animalTypeCounter = animalTypeCounter;
  }

  public int getId() {
    return id;
  }

  @Override
  public String toString() {
    //{type = “love“, smell = “turpentine“, look = “india ink“}
    return new StringJoiner(", ", "[", "]")
        .add("globalId=\"" + getGlobalId() + "\"")
        .add("id=\"" + id + "\"")
        .add("animalTypeCounter=\"" + animalTypeCounter + "\"")
        .toString();
  }
}


/*
@startuml
class Zoo extends Attraction {
- {static} EX_MESSAGE_ANIMAL_TYPE_COUNTER : String = "The animal-type-counter have to b..."
- {static} DEFAULT_ANIMAL_TYPE_COUNTER : int = 100
- {static} START_COUNTER : int = 3200
- {static} idCounter : int = START_COUNTER
- id : int
- animalTypeCounter : int
--
+ Zoo(name: String, lastYearVisitorCount int)
+ Zoo(name: String, lastYearVisitorCount: int, animalTypeCounter : int)
--
+ getAnimalTypeCounter() : int
+ setAnimalTypeCounter(animalTypeCounter : int)
+ getId() : int
+ toString() : String
}
@enduml
 */